package com.tanhua.management.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.tanhua.management.mapper.UserMapper;
import com.tanhua.management.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    public List<User> queryUserByIds(QueryWrapper queryWrapper) {

        List<User> userList = userMapper.selectList(queryWrapper);

        return userList;
    }

    public User queryUserById(QueryWrapper queryWrapper) {

        User user = userMapper.selectOne(queryWrapper);

        return user;
    }

    //冻结
    public Boolean userFreeze(Long userId, Integer freezingTime, Integer freezingRange, String reasonsForFreezing, String frozenRemarks) {

        LambdaUpdateWrapper<User> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper
                .eq(User::getId,userId) //修改的userId
                .set(User::getUpdated,new Date()) //更新修改时间
                .set(User::getFrozenTime,freezingTime)//添加冻结状态种种
                .set(User::getFrozenRange,freezingRange)
                .set(User::getFrozenReason,reasonsForFreezing)
                .set(User::getFrozenRemark,frozenRemarks)
                .set(User::getThawedReason,null); //置空解冻状态

        int result = userMapper.update(null,lambdaUpdateWrapper);

        return result == 1;
    }

    //解冻
    public Boolean userUnfreeze(Long userId, String reasonsForThawing) {

        LambdaUpdateWrapper<User> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper
                .eq(User::getId,userId) //修改的userId
                .set(User::getUpdated,new Date()) //更新修改时间
                .set(User::getFrozenTime,null)//置空冻结状态种种
                .set(User::getFrozenRange,0)
                .set(User::getFrozenReason,null)
                .set(User::getFrozenRemark,null)
                .set(User::getThawedReason,reasonsForThawing); //添加解冻状态

        int result = userMapper.update(null,lambdaUpdateWrapper);

        return result == 1;
    }
}
























