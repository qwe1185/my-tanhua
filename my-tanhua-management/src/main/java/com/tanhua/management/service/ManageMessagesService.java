package com.tanhua.management.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.dubbo.server.api.ManageMessagesApi;
import com.tanhua.dubbo.server.api.QuanZiApi;
import com.tanhua.dubbo.server.pojo.Comment;
import com.tanhua.dubbo.server.pojo.Publish;
import com.tanhua.management.enums.TotalsEnum;
import com.tanhua.management.pojo.UserInfo;
import com.tanhua.management.vo.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ManageMessagesService {

    @Reference(version = "1.0.0")
    private QuanZiApi quanZiApi;

    @Reference(version = "1.0.0")
    private ManageMessagesApi manageMessagesApi;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    //状态合计
    private List<ManageTotals> manageTotalsList(Map<String, String> param) {
        List<TotalsEnum> codes = new ArrayList<>();
        codes.add(TotalsEnum.WHOLE);
        codes.add(TotalsEnum.REVIEWED);
        codes.add(TotalsEnum.PASSED);
        codes.add(TotalsEnum.REJECTED);

        List<ManageTotals> manageTotalsList = new ArrayList<>();

        for (TotalsEnum totalsEnum : codes) {
            ManageTotals manageTotals = new ManageTotals();

            if (totalsEnum.getCode().equals("all")) {
                manageTotals.setTitle(totalsEnum.getTitle());
                manageTotals.setCode(totalsEnum.getCode());
                manageTotals.setValue(manageMessagesApi.codeCount(totalsEnum.getCode(), param));
            } else {
                manageTotals.setTitle(totalsEnum.getTitle());
                manageTotals.setCode(totalsEnum.getCode());
                manageTotals.setValue(manageMessagesApi.codeCount(totalsEnum.getCode(), param));
            }

            manageTotalsList.add(manageTotals);
        }

        return manageTotalsList;
    }

    //列表
    private List<ManageItems> manageItemsList(List<Publish> publishs) {
        List<ManageItems> manageItemsList = new ArrayList<>();

        Set<Object> userIds = publishs.stream().map(Publish::getUserId).collect(Collectors.toSet());

        LambdaQueryWrapper<UserInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(UserInfo::getUserId, userIds);
        List<UserInfo> userInfos = userInfoService.queryUserInfoList(lambdaQueryWrapper);

        //测试数据
        int reportCount = 10;
        int forwardingCount = 10;

        for (Publish publish : publishs) {
            for (UserInfo userInfo : userInfos) {
                if (publish.getUserId().intValue() == userInfo.getUserId().intValue()) {
                    ManageItems manageItems = new ManageItems();

                    manageItems.setId(publish.getId().toHexString());
                    manageItems.setNickname(userInfo.getNickName());
                    manageItems.setUserId(Math.toIntExact(userInfo.getUserId()));
                    manageItems.setUserLogo(userInfo.getLogo());
                    manageItems.setCreateDate(publish.getCreated());
                    manageItems.setText(publish.getText());

                    manageItems.setState(publish.getState());

                    manageItems.setReportCount(reportCount); //举报数
                    manageItems.setLikeCount(Math.toIntExact(quanZiApi.queryCommentCount(manageItems.getId(), 1))); //点赞数
                    manageItems.setCommentCount(Math.toIntExact(quanZiApi.queryCommentCount(manageItems.getId(), 2))); //评论数
                    manageItems.setForwardingCount(forwardingCount); //转发数

                    manageItems.setMedias(publish.getMedias().toArray(new String[]{}));

                    manageItemsList.add(manageItems);

                    reportCount++;
                    forwardingCount--;

                    break;
                }
            }
        }
        return manageItemsList;
    }

    //排序
    private List<ManageItems> manageSort(List<Publish> publishList, Map<String, String> param) {
        List<ManageItems> manageItems = manageItemsList(publishList);

        if (StringUtils.isNotEmpty(param.get("uid"))) {
            Publish publishTopState = manageMessagesApi.queryPublishByTopState(param.get("uid"));
            if (publishTopState == null) {
                return manageItemsList(publishList);
            }
            List<Publish> publishs = new ArrayList<>();
            publishs.add(publishTopState);

            if (StringUtils.equals(param.get("sortProp"), "createDate")) {
                if (CollectionUtils.isNotEmpty(publishs)) {
                    for (Publish publish : publishList) {
                        publishs.add(publish);
                    }
                }
                return CollectionUtils.isEmpty(publishs) ? manageItemsList(publishList) : manageItemsList(publishs);
            } else {
                if (StringUtils.equals(param.get("sortOrder"), "descending")) {
                    if (StringUtils.equals(param.get("sortProp"), "reportCount")) {
                        manageItems = manageItems.stream().sorted(Comparator.comparing(ManageItems::getReportCount).reversed()).collect(Collectors.toList());
                    } else if (StringUtils.equals(param.get("sortProp"), "likeCount")) {
                        manageItems = manageItems.stream().sorted(Comparator.comparing(ManageItems::getLikeCount).reversed()).collect(Collectors.toList());
                    } else if (StringUtils.equals(param.get("sortProp"), "commentCount")) {
                        manageItems = manageItems.stream().sorted(Comparator.comparing(ManageItems::getCommentCount).reversed()).collect(Collectors.toList());
                    } else if (StringUtils.equals(param.get("sortProp"), "forwardingCount")) {
                        manageItems = manageItems.stream().sorted(Comparator.comparing(ManageItems::getForwardingCount).reversed()).collect(Collectors.toList());
                    }
                }
                if (StringUtils.equals(param.get("sortOrder"), "ascending")) {
                    if (StringUtils.equals(param.get("sortProp"), "reportCount")) {
                        manageItems = manageItems.stream().sorted(Comparator.comparing(ManageItems::getReportCount)).collect(Collectors.toList());
                    } else if (StringUtils.equals(param.get("sortProp"), "likeCount")) {
                        manageItems = manageItems.stream().sorted(Comparator.comparing(ManageItems::getLikeCount)).collect(Collectors.toList());
                    } else if (StringUtils.equals(param.get("sortProp"), "commentCount")) {
                        manageItems = manageItems.stream().sorted(Comparator.comparing(ManageItems::getCommentCount)).collect(Collectors.toList());
                    } else if (StringUtils.equals(param.get("sortProp"), "forwardingCount")) {
                        manageItems = manageItems.stream().sorted(Comparator.comparing(ManageItems::getForwardingCount)).collect(Collectors.toList());
                    }
                }

                List<ManageItems> manageItemsTopState = manageItemsList(publishs);
                manageItems.forEach(items -> manageItemsTopState.add(items));

                return manageItemsTopState;
            }
        } else {
            return manageItemsList(publishList);
        }
    }

    public PageManageResult messagesList(Integer page, Integer pageSize, Map<String, String> param) {
        PageManageResult pageManageResult = new PageManageResult();
        pageManageResult.setPage(page);
        pageManageResult.setPagesize(pageSize);

        List<Publish> publishList = manageMessagesApi.publishList(page, pageSize, param);

        if (CollectionUtils.isEmpty(publishList)) {
            pageManageResult.setTotals(manageTotalsList(param));

            return pageManageResult;
        }

        pageManageResult.setCounts(StringUtils.isEmpty(param.get("state")) ? manageMessagesApi.codeCount("userMessagesCount", param) : manageMessagesApi.codeCount(param.get("state"), param));

        Integer pages = pageManageResult.getCounts() % pageManageResult.getPagesize() == 0 ? pageManageResult.getCounts() / pageManageResult.getPagesize() : pageManageResult.getCounts() / pageManageResult.getPagesize() + 1;
        pageManageResult.setPages(pages);

        pageManageResult.setItems(manageSort(publishList, param));
        pageManageResult.setTotals(manageTotalsList(param));

        return pageManageResult;
    }

    public String pass(String[] id) {
        Boolean result = manageMessagesApi.updateStatus(id, 5);

        if (result) {
            return "审核通过";
        }

        return "审核失败";
    }

    public String reject(String[] id) {
        Boolean result = manageMessagesApi.updateStatus(id, 4);

        if (result) {
            return "拒绝成功";
        }

        return "拒绝失败";
    }

    public String revocation(String[] id) {
        Boolean result = manageMessagesApi.removePublish(id);

        if (result) {
            return "撤销成功";
        }

        return "撤销失败";
    }

    public ManageMessages messages(String id) {
        Publish publish = manageMessagesApi.queryPublishById(id);

        UserInfo userInfo = userInfoService.queryUserInfoByUserId(publish.getUserId());

        ManageMessages manageMessages = new ManageMessages();

        manageMessages.setId(publish.getId().toHexString());
        manageMessages.setNickname(userInfo.getNickName());
        manageMessages.setUserId(publish.getUserId().intValue());
        manageMessages.setUserLogo(userInfo.getLogo());
        manageMessages.setCreateDate(publish.getCreated());
        manageMessages.setText(publish.getText());
        manageMessages.setMedias(publish.getMedias().toArray(new String[]{}));
        manageMessages.setState(Integer.valueOf(publish.getState()));
        manageMessages.setTopState(publish.getTopState());

        Random random = new Random();
        manageMessages.setReportCount(random.nextInt(100) + 1); //举报数
        manageMessages.setLikeCount(10L); //点赞数
        manageMessages.setCommentCount(Math.toIntExact(quanZiApi.queryCommentCount(manageMessages.getId(), 2))); //评论数
        manageMessages.setForwardingCount(random.nextInt(100) + 1); //转发数

        return manageMessages;
    }

    public PageResult comments(ObjectId publishId, String sortProp, Integer page, Integer pageSize, String sortOrder) {
        PageResult pageResult = new PageResult();

        List<Comment> comments = manageMessagesApi.queryCommentList(publishId, sortProp, page, pageSize, sortOrder);

        if (CollectionUtils.isEmpty(comments)) {
            return pageResult;
        }

        Set<Object> userIds = comments.stream().map(Comment::getUserId).collect(Collectors.toSet());

        LambdaQueryWrapper<UserInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(UserInfo::getUserId, userIds);
        List<UserInfo> userInfos = userInfoService.queryUserInfoList(lambdaQueryWrapper);

        List<CommentVo> commentVos = new ArrayList<>();
        for (Comment comment : comments) {
            for (UserInfo userInfo : userInfos) {
                if (comment.getUserId().intValue() == userInfo.getUserId().intValue()) {
                    CommentVo commentVo = new CommentVo();

                    commentVo.setId(comment.getId().toHexString());
                    commentVo.setUserId(userInfo.getUserId());
                    commentVo.setNickname(userInfo.getNickName());
                    commentVo.setContent(comment.getContent());
                    commentVo.setCreateDate(comment.getCreated());

                    commentVos.add(commentVo);

                    break;
                }
            }
        }

        pageResult.setPage(page);
        pageResult.setPagesize(pageSize);

        Integer count = Math.toIntExact(quanZiApi.queryCommentCount(publishId + "", 2));
        pageResult.setCounts(count);
        Integer pages = count % pageResult.getPagesize() == 0 ? count / pageResult.getPagesize() : count / pageResult.getPagesize() + 1;
        pageResult.setPages(pages);
        pageResult.setItems(commentVos);

        return pageResult;
    }

    public String messageTop(String id) {
        Boolean result = manageMessagesApi.messageTop(id);

        if (result) {
            return "置顶成功";
        }

        return "置顶失败";
    }

    public String messageUntop(String id) {
        Boolean result = manageMessagesApi.messageUntop(id);

        if (result) {
            return "置顶取消成功";
        }

        return "置顶取消失败";
    }
}