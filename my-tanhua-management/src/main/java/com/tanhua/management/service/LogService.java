package com.tanhua.management.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tanhua.management.mapper.LogMapper;
import com.tanhua.management.pojo.Log;
import org.apache.commons.collections.CollectionUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class LogService extends ServiceImpl<LogMapper, Log> {



    private static final Logger LOGGER = LoggerFactory.getLogger(LogService.class);


    @Autowired
    private LogMapper logMapper;

    /**
     * 通过id查询,最近活跃时间
     *
     * @param userId
     * @return
     */
    public Long queryLogLastActiveTimeById(Long userId) {
        try {
            QueryWrapper<Log> queryWrapper = new QueryWrapper();
            queryWrapper.eq("user_id", userId).orderByDesc("updated");
            List<Log> logList = this.logMapper.selectList(queryWrapper);
            if (CollectionUtils.isNotEmpty(logList)) {
                return logList.get(0).getUpdated().getTime();
            }
        } catch (Exception e) {
            LOGGER.error("查询tb_log表最近活跃时间失败");
        }
        return null;
    }

    /**
     * 通过id查询,最近活跃地点
     *
     * @param userId
     * @return
     */
    public String queryLogLastLoginLocationById(Long userId) {
        try {
            QueryWrapper<Log> queryWrapper = new QueryWrapper();
            queryWrapper.eq("user_id", userId).orderByDesc("updated");
            List<Log> logList = this.logMapper.selectList(queryWrapper);
            if (CollectionUtils.isNotEmpty(logList)) {
                return logList.get(0).getPlace();
            }
        } catch (Exception e) {
            LOGGER.error("查询tb_log表最近活跃地点失败");
        }
        return null;
    }
}
