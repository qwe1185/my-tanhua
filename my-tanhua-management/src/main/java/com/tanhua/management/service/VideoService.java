package com.tanhua.management.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.tanhua.dubbo.server.api.VideoApi;
import com.tanhua.dubbo.server.pojo.Video;
import com.tanhua.management.pojo.UserInfo;
import com.tanhua.management.vo.PageResult;
import com.tanhua.management.vo.SortProp;
import com.tanhua.management.vo.VideoPages;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VideoService {

    @Autowired
    private UserInfoService userInfoService;


    @Reference(version = "1.0.0")
    private VideoApi videoApi;

    public PageResult videosPage(String token, Long uid, Integer page, Integer pageSize, String sortProp, String sortOrder) {
        //先解析token
      /* User user = userService.queryUserByToken(token);
        if (user == null) {
            return null;
        }*/
        PageResult pageResult = new PageResult();
        pageResult.setPage(page);
        pageResult.setPagesize(pageSize);

        List<Video> allVideos = this.videoApi.findAllVideos(uid);
        if (CollectionUtils.isEmpty(allVideos)){
            return pageResult;
        }

        UserInfo userInfo = userInfoService.queryUserInfoByUserId(uid);
        VideoPages videoPages =new VideoPages();
        List<VideoPages> list = new ArrayList<>();
        for (Video video : allVideos) {
            videoPages.setId(new ObjectId());
            videoPages.setNickname(userInfo.getNickName());
            videoPages.setUserId(video.getUserId().intValue());

          videoPages.setCreateDate(video.getCreated().intValue());
          videoPages.setReportCount(RandomUtils.nextInt(0,500));
          videoPages.setLikeCount(RandomUtils.nextInt(0,500));
          videoPages.setCommentCount(RandomUtils.nextInt(0,500));
          videoPages.setForwardingCount(RandomUtils.nextInt(0,500));
            if(sortOrder.equals("ascending ")){
                PageRequest.of(page-1,pageSize, Sort.by(Sort.Order.asc("param")));
            }
            if (sortOrder.equals("descending ")){
                PageRequest.of(page-1,pageSize, Sort.by(Sort.Order.desc("param")));
            }
            videoPages.setVideoUrl(video.getVideoUrl());
            videoPages.setPicUrl(video.getPicUrl());
            list.add(videoPages);
        }



        pageResult.setItems(list);
        return pageResult;
    }
}
