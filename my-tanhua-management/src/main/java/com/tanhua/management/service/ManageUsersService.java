package com.tanhua.management.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.dubbo.server.api.UserLikeApi;
import com.tanhua.dubbo.server.api.UserLocationApi;
import com.tanhua.dubbo.server.api.VideoApi;
import com.tanhua.dubbo.server.pojo.Video;
import com.tanhua.dubbo.server.vo.UserLocationVo;
import com.tanhua.management.enums.SexEnum;
import com.tanhua.management.pojo.User;
import com.tanhua.management.pojo.UserInfo;
import com.tanhua.management.vo.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.swing.*;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class ManageUsersService {

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private UserService userService;

    @Reference(version = "1.0.0")
    private UserLikeApi userLikeApi;

    @Reference(version = "1.0.0")
    private UserLocationApi userLocationApi;

    //查询用户列表
    public PageResult queryConditionUser(String pagesize, String page, String id, String nickname, String city) {

        //1.创建返回的Vo对象
        PageResult pageResult = new PageResult();
        if (StringUtils.isNotEmpty(page)) {
            pageResult.setPage(Integer.valueOf(page));
        }
        if (StringUtils.isNotEmpty(pagesize)) {
            pageResult.setPage(Integer.valueOf(pagesize));
        }
        pageResult.setPages(0);
        pageResult.setCounts(0);

        //2.得到MongoDB的数据
        List<UserLocationVo> userLocationVoList = new ArrayList<UserLocationVo>();
        if (StringUtils.isEmpty(id)) {
            userLocationVoList = userLocationApi.queryAllUserLocation();
        } else {
            UserLocationVo userLocationVo = userLocationApi.queryByUserId(Long.valueOf(id));
            //如果查询id为空,返回空
            if (userLocationVo == null) {
                return pageResult;
            }

            userLocationVoList.add(userLocationVo);
        }

        if (CollectionUtils.isEmpty(userLocationVoList)) {
            //如果没有数据直接返回空的PageResult
            return pageResult;
        }

        //3.得到mongoDB的所有用户id根据用户得到其所有详细信息
        HashSet<Long> userIds = new HashSet<>();
        for (UserLocationVo userLocationVo : userLocationVoList) {
            userIds.add(userLocationVo.getUserId()); //TODO
        }

        //4.1得到MySql的详细数据UserInfo
        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("user_id", userIds);
        if (StringUtils.isNotEmpty(nickname)) {
            queryWrapper.like("nick_name", nickname);
        }
        if (StringUtils.isNotEmpty(city)) {
            queryWrapper.like("city", city);
        }

        List<UserInfo> userInfoList = userInfoService.queryUserInfoList(queryWrapper);


        //4.2查询Mysql得到另一张表User得到手机号
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.in("id", userIds);
        List<User> userList = userService.queryUserByIds(userQueryWrapper);


        //5.Vo数据整合UserInfo
        ArrayList<UserBasicVo> userBasicVolist = new ArrayList<>();
        for (UserLocationVo userLocationVo : userLocationVoList) {
            for (UserInfo userInfo : userInfoList) {
                if (userLocationVo.getUserId().longValue() == userInfo.getUserId().longValue()) {
                    UserBasicVo userBasicVo = new UserBasicVo();

                    userBasicVo.setId(Integer.valueOf(userInfo.getUserId().toString()));
                    userBasicVo.setLogo(userInfo.getLogo());
                    userBasicVo.setLogoStatus("1"); //填充数据
                    userBasicVo.setNickname(userInfo.getNickName());
                    //userBasicVo.setMobile();
                    userBasicVo.setSex(userInfo.getSex().toString());
                    userBasicVo.setAge(userInfo.getAge());
                    userBasicVo.setOccupation(userInfo.getIndustry());
                    userBasicVo.setLastActiveTime(userLocationVo.getLastUpdated());
                    userBasicVo.setCity(userInfo.getCity());

                    userBasicVolist.add(userBasicVo);
                    break;
                }

            }

        }

        //6.Vo数据整合User
        for (UserBasicVo userBasicVo : userBasicVolist) {
            for (User user : userList) {
                if (userBasicVo.getId().longValue() == user.getId().longValue()) {
                    userBasicVo.setMobile(user.getMobile());
                    userBasicVo.setUserStatus(user.getFrozenRange()==0?"1":"2"); //用户冻结范围
                    break;
                }
            }

        }

        //7.将数据填入返回的Vo
        pageResult.setItems(userBasicVolist);
        return pageResult;
    }

    //查询单个用户详情
    public UserDetailVo queryUserDetailed(String userId) {
        UserDetailVo userDetailVo = new UserDetailVo();

        //查询User
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("id", userId);
        User user = userService.queryUserById(userQueryWrapper);

        Long id = user.getId(); //用户ID
        String mobile = user.getMobile(); //手机号
        Integer frozenRange = user.getFrozenRange();//用户冻结范围
        Date created = user.getCreated(); //注册时间

        userDetailVo.setId(id.intValue());
        userDetailVo.setMobile(mobile);
        userDetailVo.setUserStatus(frozenRange==0?"1":"2");
        long time = created.getTime(); //将Date数据类型转换成long型
        userDetailVo.setCreated(time);
        //查询UserInfo
        UserInfo userInfo = userInfoService.queryUserInfoByUserId(Long.valueOf(userId));

        String nickName = userInfo.getNickName(); //昵称
        SexEnum sex = userInfo.getSex(); //性别
        Integer age = userInfo.getAge(); //年龄
        String income = userInfo.getIncome(); //收入
        String industry = userInfo.getIndustry(); //当作行业
        String city = userInfo.getCity(); //注册地区
        String tags = userInfo.getTags(); //用户标签
        String logo = userInfo.getLogo(); //头先地址

        userDetailVo.setNickname(nickName);
        userDetailVo.setSex(sex.toString());
        userDetailVo.setAge(age);
        userDetailVo.setIncome(Integer.valueOf(income));
        userDetailVo.setOccupation(industry);
        userDetailVo.setCity(city);
        userDetailVo.setTags(tags);
        userDetailVo.setLogo(logo);

        //查询mongoDB user_like
        Long countLiked = userLikeApi.queryLikeCount(Long.valueOf(userId)); //喜欢数
        Long countBeLiked = userLikeApi.queryFanCount(Long.valueOf(userId)); //被喜欢数
        Long countMatching = userLikeApi.queryEachLikeCount(Long.valueOf(userId)); //配对数

        userDetailVo.setCountLiked(countLiked.intValue());
        userDetailVo.setCountBeLiked(countBeLiked.intValue());
        userDetailVo.setCountMatching(countMatching.intValue());
        //查询user_location
        UserLocationVo userLocationVo = userLocationApi.queryByUserId(Long.valueOf(userId));
        Long lastActiveTime = userLocationVo.getLastUpdated(); //最近活跃时间
        String lastLoginLocation = userLocationVo.getAddress(); //最近登录地点

        userDetailVo.setLastActiveTime(lastActiveTime);
        userDetailVo.setLastLoginLocation(lastLoginLocation);
        // TODO 个性签名随便设置
        userDetailVo.setPersonalSignature("查的太尼玛累了!");

        return userDetailVo;
    }

    public Map<String, String> userFreeze(Map<String, String> param) {

        HashMap<String, String> message = new HashMap<>();

        String userId = param.get("userId");
        String freezingTime = param.get("freezingTime");
        String freezingRange = param.get("freezingRange");
        String reasonsForFreezing = param.get("reasonsForFreezing");
        String frozenRemarks = param.get("frozenRemarks");

        Boolean bool = userService.userFreeze(Long.valueOf(userId), Integer.valueOf(freezingTime), Integer.valueOf(freezingRange), reasonsForFreezing, frozenRemarks);
        if (!bool) {
            message.put("message", "操作失败!");
            return message;
        }

        message.put("message", "操作成功!");
        return message;
    }


    public Map<String, String> userUnfreeze(Map<String, String> param) {
        HashMap<String, String> message = new HashMap<>();

        String userId = param.get("userId");
        String reasonsForThawing = param.get("reasonsForThawing");
        System.out.println("reasonsForThawing:"+reasonsForThawing);

        Boolean bool = userService.userUnfreeze(Long.valueOf(userId), reasonsForThawing);
        if (!bool) {
            message.put("message", "操作失败!");
            return message;
        }

        message.put("message", "操作成功!");
        return message;
    }



    }










