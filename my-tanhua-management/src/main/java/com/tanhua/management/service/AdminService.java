package com.tanhua.management.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanhua.management.mapper.AdminMapper;

import com.tanhua.management.pojo.Admin;

import com.tanhua.management.vo.AdminVo;
import io.jsonwebtoken.*;
import jdk.nashorn.internal.parser.Token;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import sun.java2d.pipe.SpanIterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class AdminService {


    @Autowired
    private AdminMapper adminMapper;

    @Value("${jwt.secret}")
    private String secret;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Autowired
    private AdminService adminService;

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private static final String redisKey = "CACHE_KEY_TOKEN_LOGIN_";

    /**
     * 验证码
     */
    private static final String CACHE_CODE_PREFIX = "MANAGE_CAP_";

    //图片验证码
    public void saveVerification(String uuid, String codePic) {
        //将验证码存入redis,设置有效时间为5分钟
        String redisKey = CACHE_CODE_PREFIX + uuid;
        redisTemplate.opsForValue().set(redisKey, codePic, Duration.ofMinutes(5));
    }

    /**
     * 用户登录
     *
     * @param username
     * @param password
     * @param code
     * @param uuid
     * @return java.util.Map
     * @date 2021/3/27 0027 15:35
     * @author Administrator
     */
    public Map login(String username, String password, String code, String uuid) {

        //先判断用户名和密码是否存在,在进行查询
        if (StringUtils.isEmpty(username)) {
            return null;
        }
        if (StringUtils.isEmpty(password)) {
            return null;
        }

        //查询数据库
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", username);
        Admin admin = adminMapper.selectOne(queryWrapper);
        if (null == admin) {
            return null;
        }
        String pwd = DigestUtils.md5Hex(password);
        if (!StringUtils.equals(pwd, admin.getPassword())) {
            return null;
        }
        String redisKey = CACHE_CODE_PREFIX + uuid;

        String value = redisTemplate.opsForValue().get(redisKey);
        if (StringUtils.isEmpty(value)) {
            return null;
        }

        if (!StringUtils.equalsIgnoreCase(value, code)) {
            // 如果从redis中取出的验证码与用户输入的不一致，验证码输入错误
            return null;
        }

        // 验证完毕后删除验证码
        redisTemplate.delete(redisKey);
        // 将账号和用户id封装成map集合，然后生成token
        Map<String, Object> map = new HashMap<>();
        map.put("username", admin.getUsername());
        map.put("id", admin.getId());

        //生成token
        String token = Jwts.builder().setClaims(map)
                .signWith(SignatureAlgorithm.HS256, secret)
                .setExpiration(new DateTime().plusHours(12).toDate())
                .compact();
        System.out.println(token+"登录生成的token");
        try {
            //将token存入redis中
            String redisTokenKey = AdminService.redisKey + token;
            admin.setPassword(null);
            String redisTokenKeyValue = MAPPER.writeValueAsString(admin);
            redisTemplate.opsForValue().set(redisTokenKey,redisTokenKeyValue);
            Map hashMap = new HashMap<>();
            hashMap.put("token",token);
            return hashMap;
        } catch (Exception e) {
            log.error("存储token出错!",e);
            return null;
        }



    }

    //根据用户名查询token的方法
    public Admin queryAdminByToken(String token) {

//        try {
//            //通过token解析数据
//            Map<String, Object> body = Jwts.parser()
//                    .setSigningKey(secret)
//                    .parseClaimsJws(token)
//                    .getBody();
//            Long id = Long.valueOf(body.get("id").toString());
//            Admin admin = new Admin();
//            admin.setId(id);
//            //需要查询数据库获取用户为了不影响性能对手机好做缓存处理
//            String redisKey = "CACHE_KEY_TOKEN_ADMIN" + admin.getId();
//            //判断redis中是否过期
//            if (redisTemplate.hasKey(redisKey)) {
//
//                String nameValue = redisTemplate.opsForValue().get(redisKey);
//                admin.setUsername(nameValue);
//            } else {
//
//                //查询数据库
//                Admin admin1 = adminMapper.selectById(admin.getId());
//                admin.setUsername(admin1.getUsername());
//                //将用户写入到redis中并设置过期时间与token 过期时间保持一致
//                long timeout = Long.valueOf(body.get("exp").toString()) * 1000 - System.currentTimeMillis();
//                redisTemplate.opsForValue().set(redisKey, admin1.getUsername(), timeout, TimeUnit.MILLISECONDS);
//                return admin;
//            }
//            return adminMapper.selectById(id);
//        } catch (ExpiredJwtException e) {
//            log.info("token已经过期" + token, e);
//        } catch (Exception e) {
//            log.error("token不合法" + token, e);
//        }
        try {

            //String newToken = token.replace("Bearer ", "");

            // 通过token解析数据
            Map<String, Object> body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
            Long id = Long.valueOf(body.get("id").toString());
            Admin user = new Admin();
            user.setId(id);
            //先查缓存
            String redisKey = AdminService.redisKey + user.getId();
            if (this.redisTemplate.hasKey(redisKey)) {
                String username = this.redisTemplate.opsForValue().get(redisKey);
                user.setUsername(username);
            } else {
                user = adminMapper.selectById(id);
                //手机号放到redis，还得和token的过期时间保持一致
                String exp = body.get("exp").toString();//秒为单位
                Long timeout = Long.valueOf(exp) * 1000 - System.currentTimeMillis();
                this.redisTemplate.opsForValue().set(redisKey, user.getUsername(), timeout, TimeUnit.MILLISECONDS);
            }
            return user;
        } catch (Exception e) {
            log.error("解析用户出错", e);
            return null;
        }

    }

    //用户基本信息
    public AdminVo saveAdminVo(String newToken) {
        //System.out.println(token);

        Admin admin = adminService.queryAdminByToken(newToken);
        if (null != admin) {
            AdminVo adminVo = new AdminVo();
            adminVo.setAvatar(admin.getAvatar());
            adminVo.setUsername(admin.getUsername());
            adminVo.setUid(admin.getId().toString());

            return adminVo;
        }
        return null;

    }

    //登出
    public Boolean loginOut(String token) {


        String redisKey = AdminService.redisKey + token;
        String value = redisTemplate.opsForValue().get(redisKey);
        if (null!=value){

            return redisTemplate.delete(redisKey);
        }
        return false;
    }

}
