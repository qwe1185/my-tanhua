package com.tanhua.management.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.management.enums.AreaEnum;
import com.tanhua.management.enums.SexEnum;
import com.tanhua.management.mapper.LogMapper;
import com.tanhua.management.mapper.UserInfoMapper;
import com.tanhua.management.mapper.UsersMapper;
import com.tanhua.management.pojo.Log;
import com.tanhua.management.pojo.UserInfo;
import com.tanhua.management.pojo.Users;
import com.tanhua.management.utils.SbDates;
import com.tanhua.management.vo.DistributionVo;
import com.tanhua.management.vo.UserActivityVo;
import com.tanhua.management.vo.UsersVo;
import com.tanhua.management.vo.YearVo;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tanhua.management.enums.AgeRangeEnum;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class DashboardService {


    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private LogMapper logMapper;

    @Autowired
    private UsersMapper usersMapper;

    //查询用户活跃数据
    public UserActivityVo summmaryQuery() {
        UserActivityVo userActivityVo = new UserActivityVo();
        int cumulativeUsers = 1;
        int activePassMonth = 1;
        int activePassWeek = 1;
        int newUsersToday = 1;
        int newUsersTodayRate = 1;
        int loginTimesToday = 1;
        int loginTimesTodayRate = 1;
        int activeUsersToday = 1;
        int activeUsersTodayRate = 1;
        int useTimePassWeek = 1;
        int activeUsersYesterday = 1;
        int activeUsrsYesterdayRate = 1;


        //获取累计用户总量
        cumulativeUsers = userInfoMapper.selectCount(new QueryWrapper<>());
        System.out.println(cumulativeUsers);
        //获取过去30天和7天活跃用户
        //当日00点
        Date endTime = SbDates.getStartTime();
        long endMillis = endTime.getTime();
        //明天00:00
        Date startTimeDay = new Date(endMillis + (24 * 60 * 60 * 1000L));
        //昨天00:00
        Date startTimeDay2 = new Date(endMillis - (24 * 60 * 60 * 1000L));
        //前天00:00
        Date startTimeDay3 = new Date(endMillis - (48 * 60 * 60 * 1000L));
        //七天前00:00
        Date startTime7 = new Date(endMillis - (7 * 24 * 60 * 60 * 1000L));
        //30天谴00:00
        Date startTime30 = new Date(endMillis - (30 * 24 * 60 * 60 * 1000L));
        //7天活跃
        QueryWrapper<Log> startTime7Wrapper = new QueryWrapper<>();
        startTime7Wrapper.eq("type", "01").between("created", startTime7, endTime);
        activePassWeek = logMapper.selectCount(startTime7Wrapper);
        //30天活跃
        QueryWrapper<Log> startTime30Wrapper = new QueryWrapper<>();
        startTime30Wrapper.eq("type", "01").between("created", startTime30, endTime);
        activePassMonth = logMapper.selectCount(startTime30Wrapper);
        //今日新增
        QueryWrapper<UserInfo> startTimeDayWrapper = new QueryWrapper<>();
        startTimeDayWrapper.between("created", startTimeDay, endTime);
        newUsersToday = userInfoMapper.selectCount(startTimeDayWrapper);
        //环比增长率
        QueryWrapper<UserInfo> startTimeDay2Wrapper = new QueryWrapper<>();
        startTimeDay2Wrapper.between("created", startTimeDay2, endTime);
        int newUsersYesterday = userInfoMapper.selectCount(startTimeDay2Wrapper);
        newUsersYesterday = (newUsersToday / (newUsersYesterday + 1)) / 100 ;
        //今日登陆总次数
        QueryWrapper<Log> startTimeDayLogWrapper = new QueryWrapper<>();
        startTimeDayLogWrapper.eq("type", "01").between("created", startTimeDay, endTime);
        loginTimesToday = logMapper.selectCount(startTimeDayLogWrapper);
        //昨日登陆次数以及今日环比
        QueryWrapper<Log> startTimeDayLog2Wrapper = new QueryWrapper<>();
        startTimeDayLog2Wrapper.eq("type", "01").between("created", startTimeDay2, endTime);
        int loginTimesYesterday = logMapper.selectCount(startTimeDayLog2Wrapper);
        loginTimesTodayRate = (loginTimesToday / (loginTimesYesterday + 1)) / 100 ;
        //今日登陆用户数
        //复用今日登陆次数的查询构造
        List<Log> logList = logMapper.selectList(startTimeDayLogWrapper);
        Set<Integer> userIdsToday = new HashSet<>();
        for (Log log : logList) {
            userIdsToday.add(log.getUser_id());
        }
        activeUsersToday = userIdsToday.size();
        //昨日登陆用户数(活跃)以及今日活跃环比
        List<Log> logListYes = logMapper.selectList(startTimeDayLog2Wrapper);
        Set<Integer> userIdsYes = new HashSet<>();
        for (Log log : logListYes) {
            userIdsYes.add(log.getUser_id());
        }
        activeUsersYesterday = userIdsYes.size();
        activeUsersTodayRate = (activeUsersToday / (activeUsersYesterday + 1)) / 100 ;
        //昨日活跃环比
        QueryWrapper<Log> startTimeDayLog3Wrapper = new QueryWrapper<>();
        startTimeDayLog3Wrapper.eq("type", "01").between("created", startTimeDay2, startTimeDay3);
        List<Log> logListDay2 = logMapper.selectList(startTimeDayLog3Wrapper);
        Set<Integer> userIdsDay2 = new HashSet<>();
        for (Log log : logListDay2) {
            userIdsDay2.add(log.getUser_id());
        }

        int activeUsersYesterdayRate = (activeUsersYesterday / (userIdsDay2.size() + 1)) / 100 ;
        //每周平均使用时长 TO DO
        //封装vo
        userActivityVo.setCumulativeUsers(cumulativeUsers);
        userActivityVo.setActivePassMonth(activePassMonth);        //activePassMonth = 0;
        userActivityVo.setActivePassWeek(activePassWeek);        //activePassWeek = 0;
        userActivityVo.setNewUsersToday(newUsersToday);        //newUsersToday = 0;
        userActivityVo.setNewUsersTodayRate(newUsersTodayRate);        //newUsersTodayRate = 0;
        userActivityVo.setLoginTimesToday(loginTimesToday);        //loginTimesToday = 0;
        userActivityVo.setLoginTimesTodayRate(loginTimesTodayRate);        //loginTimesTodayRate = 0;
        userActivityVo.setActiveUsersToday(activeUsersToday);        //activeUsersToday = 0;
        userActivityVo.setActiveUsersTodayRate(activeUsersTodayRate);        //activeUsersTodayRate = 0
        userActivityVo.setUseTimePassWeek(useTimePassWeek);        //            TO  DO            useTimePassWeek = 0;
        userActivityVo.setActiveUsersYesterday(activeUsersYesterday);        //activeUsersYesterday = 0
        userActivityVo.setActiveUsersYesterdayRate(activeUsersYesterdayRate);        //activeUsersYesterdayRate
        return userActivityVo;
    }


    /**
     * 分布
     *
     * @param sd
     * @param ed
     * @return
     */
    public DistributionVo distributionQuery(Long sd, Long ed) {

        //获取所有用户集合


        String startDate = new DateTime(sd).toString("yyyy-MM-dd HH:mm:ss");
        String endDate = new DateTime(ed).toString("yyyy-MM-dd HH:mm:ss");
/*     Date startDate = new Date(sd);
     Date endDate = new Date(ed);*/
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
        wrapper.between("created", startDate, endDate);
        List<UserInfo> userInfoList = userInfoMapper.selectList(wrapper);
        DistributionVo distributionVo = new DistributionVo();
        //职业分布
        List<YearVo> industryDistribution = new ArrayList<>();
        int fuwu = userInfoList.stream().filter(userInfo -> userInfo.getIndustry().equals("服务")).collect(Collectors.toList()).size();
        int wenyu = userInfoList.stream().filter(userInfo -> userInfo.getIndustry().equals("文娱")).collect(Collectors.toList()).size();
        int canyin = userInfoList.stream().filter(userInfo -> userInfo.getIndustry().equals("餐饮")).collect(Collectors.toList()).size();
        int jiaoyu = userInfoList.stream().filter(userInfo -> userInfo.getIndustry().equals("教育")).collect(Collectors.toList()).size();
        int jinrong = userInfoList.stream().filter(userInfo -> userInfo.getIndustry().equals("金融")).collect(Collectors.toList()).size();
        int zhusu = userInfoList.stream().filter(userInfo -> userInfo.getIndustry().equals("住宿")).collect(Collectors.toList()).size();
        int dichan = userInfoList.stream().filter(userInfo -> userInfo.getIndustry().equals("地产")).collect(Collectors.toList()).size();
        int wuliu = userInfoList.stream().filter(userInfo -> userInfo.getIndustry().equals("物流")).collect(Collectors.toList()).size();
        int zhizao = userInfoList.stream().filter(userInfo -> userInfo.getIndustry().equals("制造")).collect(Collectors.toList()).size();
        int yiliao = userInfoList.stream().filter(userInfo -> userInfo.getIndustry().equals("医疗")).collect(Collectors.toList()).size();
        industryDistribution.add(new YearVo("服务", fuwu));
        industryDistribution.add(new YearVo("文娱", wenyu));
        industryDistribution.add(new YearVo("餐饮", canyin));
        industryDistribution.add(new YearVo("教育", jiaoyu));
        industryDistribution.add(new YearVo("金融", jinrong));
        industryDistribution.add(new YearVo("地产", dichan));
        industryDistribution.add(new YearVo("物流", wuliu));
        industryDistribution.add(new YearVo("制造", zhizao));
        industryDistribution.add(new YearVo("医疗", yiliao));
        industryDistribution.add(new YearVo("住宿", zhusu));

        //年龄分布
        List<YearVo> ageDistribution = new ArrayList<>();
        ageDistribution.add(new YearVo(AgeRangeEnum.UNDER_TWENTY.toString(), userInfoList.stream().filter(userInfo -> userInfo.getAge() >= 0 && userInfo.getAge() < 20).collect(Collectors.toList()).size()));
        ageDistribution.add(new YearVo(AgeRangeEnum.TWENTY.toString(), userInfoList.stream().filter(userInfo -> userInfo.getAge() >= 20 && userInfo.getAge() < 30).collect(Collectors.toList()).size()));
        ageDistribution.add(new YearVo(AgeRangeEnum.THIRTY.toString(), userInfoList.stream().filter(userInfo -> userInfo.getAge() >= 30 && userInfo.getAge() < 40).collect(Collectors.toList()).size()));
        ageDistribution.add(new YearVo(AgeRangeEnum.FORTY.toString(), userInfoList.stream().filter(userInfo -> userInfo.getAge() >= 40 && userInfo.getAge() < 50).collect(Collectors.toList()).size()));
        ageDistribution.add(new YearVo(AgeRangeEnum.OVER_FIFTY.toString(), userInfoList.stream().filter(userInfo -> userInfo.getAge() >= 50 && userInfo.getAge() < 200).collect(Collectors.toList()).size()));

        //性别分布
        List<YearVo> genderDistribution = new ArrayList<>();
        int countMan = 0;
        int countWoman = 0;
        countMan = userInfoList.stream().filter(userInfo -> (userInfo.getSex() == SexEnum.MAN)).collect(Collectors.toList()).size();
        countWoman = userInfoList.size() - countMan;
        genderDistribution.add(new YearVo("男性用户", countMan));
        genderDistribution.add(new YearVo("女性用户", countWoman));

        //省份分布
        List<YearVo> localDistribution = new ArrayList<>();
        localDistribution.add(new YearVo(AreaEnum.HUBEI.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.HUBEI.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.HUNAN.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.HUNAN.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.HENAN.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.HENAN.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.SHANXI.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.SHANXI.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.HEBEI.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.HEBEI.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.BEIJING.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.BEIJING.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.TIANJIN.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.TIANJIN.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.NEIMENGGU.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.NEIMENGGU.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.ZHEJIANG.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.ZHEJIANG.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.ANHUI.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.ANHUI.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.SHANGHAI.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.SHANGHAI.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.JIANGXI.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.JIANGXI.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.SHANDONG.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.SHANDONG.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.JIANGSU.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.JIANGSU.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.FUJIAN.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.FUJIAN.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.TAIWAN.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.TAIWAN.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.GUANGXI.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.GUANGXI.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.GUANGDONG.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.GUANGDONG.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.HAINAN.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.HAINAN.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.HONGKONG.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.HONGKONG.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.MACAO.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.MACAO.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.QINGHAI.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.QINGHAI.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.NINGXIA.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.NINGXIA.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.SHANXI2.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.SHANXI2.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.GANSU.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.GANSU.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.XINJIANG.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.XINJIANG.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.JILIN.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.JILIN.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.HEILONGJIANG.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.HEILONGJIANG.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.LIAONING.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.LIAONING.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.GUIZHOU.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.GUIZHOU.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.YUNNAN.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.YUNNAN.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.CHONGQING.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.CHONGQING.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.SICHUAN.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.SICHUAN.getProvince());
        }).collect(Collectors.toList()).size()));
        localDistribution.add(new YearVo(AreaEnum.TIBET.getProvince(), userInfoList.stream().filter(userInfo -> {
            String[] city = userInfo.getCity().split("-");
            return city[0].startsWith(AreaEnum.TIBET.getProvince());
        }).collect(Collectors.toList()).size()));

        //地区分布
        List<YearVo> localTotal = new ArrayList<>();
        localTotal.add(new YearVo("华中地区", localDistribution.get(0).getAmount() + localDistribution.get(1).getAmount() + localDistribution.get(2).getAmount()));
        localTotal.add(new YearVo("华北地区", localDistribution.get(3).getAmount() + localDistribution.get(4).getAmount() + localDistribution.get(5).getAmount() + localDistribution.get(6).getAmount()));
        localTotal.add(new YearVo("华东地区", localDistribution.get(8).getAmount() + localDistribution.get(9).getAmount() + localDistribution.get(10).getAmount() + localDistribution.get(11).getAmount() + localDistribution.get(12).getAmount() + localDistribution.get(13).getAmount() + localDistribution.get(14).getAmount() + localDistribution.get(15).getAmount()));
        localTotal.add(new YearVo("华南地区", localDistribution.get(16).getAmount() + localDistribution.get(17).getAmount() + localDistribution.get(18).getAmount() + localDistribution.get(19).getAmount() + localDistribution.get(20).getAmount()));
        localTotal.add(new YearVo("西北地区", localDistribution.get(21).getAmount() + localDistribution.get(22).getAmount() + localDistribution.get(23).getAmount() + localDistribution.get(24).getAmount() + localDistribution.get(25).getAmount()));
        localTotal.add(new YearVo("东北地区", localDistribution.get(26).getAmount() + localDistribution.get(27).getAmount() + localDistribution.get(28).getAmount()));
        localTotal.add(new YearVo("西南地区", localDistribution.get(29).getAmount() + localDistribution.get(30).getAmount() + localDistribution.get(31).getAmount() + localDistribution.get(32).getAmount() + localDistribution.get(3).getAmount()));


        distributionVo.setIndustryDistribution(industryDistribution);
        distributionVo.setAgeDistribution(ageDistribution);
        distributionVo.setGenderDistribution(genderDistribution);
        distributionVo.setLocalDistribution(localDistribution);
        distributionVo.setLocalTotal(localTotal);
        return distributionVo;
    }

    public UsersVo usersQuery(Long sd, Long ed, Integer type) {
        UsersVo usersVo = new UsersVo();
        switch (type) {
            case 101: {
                usersVo = this.newUsersQuery(sd, ed);
                break;
            }
            case 102: {
                usersVo = this.livelyUsersQuery(sd, ed);
                break;
            }
            case 103: {
                usersVo = this.aliveUsersQuery(sd, ed);
                break;
            }
            default: {
                usersVo = null;
                break;
            }
        }
        return usersVo;
    }


    /**
     * 次日留存率
     *
     * @param sd
     * @param ed
     * @return
     */
    private UsersVo aliveUsersQuery(Long sd, Long ed) {
        Date sDate = new Date(sd);
        DateTime stDate = new DateTime(sd);
        Date eDate = new Date(ed);
        DateTime edDate = new DateTime(ed);

        //空对象
        UsersVo usersVo = new UsersVo();
        List<YearVo> thisYear = new ArrayList<>();
        List<YearVo> lastYear = new ArrayList<>();
        thisYear.add(new YearVo("1月", 66));
        thisYear.add(new YearVo("2月", 68));
        thisYear.add(new YearVo("3月", 81));
        thisYear.add(new YearVo("4月", 50));
        thisYear.add(new YearVo("5月", 84));
        thisYear.add(new YearVo("6月", 15));
        thisYear.add(new YearVo("7月", 48));
        thisYear.add(new YearVo("8月", 85));
        thisYear.add(new YearVo("9月", 48));
        thisYear.add(new YearVo("10月", 78));
        thisYear.add(new YearVo("11月", 95));
        thisYear.add(new YearVo("12月", 48));
        lastYear.add(new YearVo("1月", 91));
        lastYear.add(new YearVo("2月", 48));
        lastYear.add(new YearVo("3月", 78));
        lastYear.add(new YearVo("4月", 80));
        lastYear.add(new YearVo("5月", 45));
        lastYear.add(new YearVo("6月", 99));
        lastYear.add(new YearVo("7月", 29));
        lastYear.add(new YearVo("8月", 87));
        lastYear.add(new YearVo("9月", 78));
        lastYear.add(new YearVo("10月", 11));
        lastYear.add(new YearVo("11月", 28));
        lastYear.add(new YearVo("12月", 47));

        //获取集合
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        wrapper.eq("record_date", sDate);
        List<Users> UsersList = usersMapper.selectList(wrapper);

        Calendar cal = Calendar.getInstance();
        //准备好返回对象
        //间隔天数
        int days = Days.daysBetween(stDate, edDate).getDays();
        if (days < 31) {

            cal.setTime(sDate);
            int month = cal.get(Calendar.MONTH) + 1;
            thisYear.set(month - 1, new YearVo(month + "月", UsersList.size()));
            lastYear.set(month - 1, new YearVo(month + "月", UsersList.size() - 5));
        } else {

        }
        usersVo.setLastYear(lastYear);
        usersVo.setThisYear(thisYear);
        return usersVo;
    }

    /**
     * 活跃用户数
     *
     * @param sd
     * @param ed
     * @return
     */
    private UsersVo livelyUsersQuery(Long sd, Long ed) {
        Date sDate = new Date(sd);
        DateTime stDate = new DateTime(sd);
        Date eDate = new Date(ed);
        DateTime edDate = new DateTime(ed);

        //空对象
        UsersVo usersVo = new UsersVo();
        List<YearVo> thisYear = new ArrayList<>();
        List<YearVo> lastYear = new ArrayList<>();
        thisYear.add(new YearVo("1月", 50205));
        thisYear.add(new YearVo("2月", 26315));
        thisYear.add(new YearVo("3月", 34454));
        thisYear.add(new YearVo("4月", 52467));
        thisYear.add(new YearVo("5月", 23345));
        thisYear.add(new YearVo("6月", 23454));
        thisYear.add(new YearVo("7月", 21434));
        thisYear.add(new YearVo("8月", 42572));
        thisYear.add(new YearVo("9月", 23845));
        thisYear.add(new YearVo("10月", 52434));
        thisYear.add(new YearVo("11月", 98875));
        thisYear.add(new YearVo("12月", 12434));
        lastYear.add(new YearVo("1月", 43566));
        lastYear.add(new YearVo("2月", 23156));
        lastYear.add(new YearVo("3月", 24530));
        lastYear.add(new YearVo("4月", 12834));
        lastYear.add(new YearVo("5月", 23445));
        lastYear.add(new YearVo("6月", 23556));
        lastYear.add(new YearVo("7月", 76516));
        lastYear.add(new YearVo("8月", 12334));
        lastYear.add(new YearVo("9月", 81236));
        lastYear.add(new YearVo("10月", 38145));
        lastYear.add(new YearVo("11月", 44353));
        lastYear.add(new YearVo("12月", 12835));

        //获取集合
        QueryWrapper<Log> wrapper = new QueryWrapper<>();
        wrapper.between("log_time", sDate, eDate);
        List<Log> LogList = logMapper.selectList(wrapper);

        Calendar cal = Calendar.getInstance();
        //准备好返回对象
        //间隔天数
        int days = Days.daysBetween(stDate, edDate).getDays();
        if (days < 31) {

            cal.setTime(sDate);
            int month = cal.get(Calendar.MONTH) + 1;
            thisYear.set(month - 1, new YearVo(month + "月", LogList.size()));
            lastYear.set(month - 1, new YearVo(month + "月", LogList.size() - 500));
        } else {

        }
        usersVo.setLastYear(lastYear);
        usersVo.setThisYear(thisYear);
        return usersVo;
    }

    /**
     * 新增用户
     *
     * @param sd
     * @param ed
     * @return
     */
    private UsersVo newUsersQuery(Long sd, Long ed) {
        Date sDate = new Date(sd);
        DateTime stDate = new DateTime(sd);
        Date eDate = new Date(ed);
        DateTime edDate = new DateTime(ed);

        //空对象
        UsersVo usersVo = new UsersVo();
        List<YearVo> thisYear = new ArrayList<>();
        List<YearVo> lastYear = new ArrayList<>();
        thisYear.add(new YearVo("1月", 5005));
        thisYear.add(new YearVo("2月", 2615));
        thisYear.add(new YearVo("3月", 3454));
        thisYear.add(new YearVo("4月", 5467));
        thisYear.add(new YearVo("5月", 2345));
        thisYear.add(new YearVo("6月", 2354));
        thisYear.add(new YearVo("7月", 2134));
        thisYear.add(new YearVo("8月", 4252));
        thisYear.add(new YearVo("9月", 2345));
        thisYear.add(new YearVo("10月", 5234));
        thisYear.add(new YearVo("11月", 9875));
        thisYear.add(new YearVo("12月", 1234));
        lastYear.add(new YearVo("1月", 4366));
        lastYear.add(new YearVo("2月", 2356));
        lastYear.add(new YearVo("3月", 2430));
        lastYear.add(new YearVo("4月", 1234));
        lastYear.add(new YearVo("5月", 2345));
        lastYear.add(new YearVo("6月", 2356));
        lastYear.add(new YearVo("7月", 7656));
        lastYear.add(new YearVo("8月", 1234));
        lastYear.add(new YearVo("9月", 1236));
        lastYear.add(new YearVo("10月", 3145));
        lastYear.add(new YearVo("11月", 4353));
        lastYear.add(new YearVo("12月", 1235));

        //获取集合
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
        wrapper.between("created", sDate, eDate);
        List<UserInfo> userInfoList = userInfoMapper.selectList(wrapper);

        Calendar cal = Calendar.getInstance();
        //准备好返回对象
        //间隔天数
        int days = Days.daysBetween(stDate, edDate).getDays();
        if (days < 31) {

            cal.setTime(sDate);
            int month = cal.get(Calendar.MONTH) + 1;
            thisYear.set(month - 1, new YearVo(month + "月", userInfoList.size()));
            lastYear.set(month - 1, new YearVo(month + "月", userInfoList.size() - 20));
        } else {

        }
        usersVo.setLastYear(lastYear);
        usersVo.setThisYear(thisYear);
        return usersVo;
    }
}
