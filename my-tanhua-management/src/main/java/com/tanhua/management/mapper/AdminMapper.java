package com.tanhua.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.management.pojo.Admin;


public interface AdminMapper extends BaseMapper<Admin> {
}
