package com.tanhua.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.management.pojo.Log;

import org.springframework.stereotype.Repository;

@Repository
public interface LogMapper extends BaseMapper<Log> {

}
