package com.tanhua.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.management.pojo.UserInfo;


public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
