package com.tanhua.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.management.pojo.Users;

public interface UsersMapper extends BaseMapper<Users> {
}
