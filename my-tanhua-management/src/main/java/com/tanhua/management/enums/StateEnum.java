package com.tanhua.management.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum StateEnum implements IEnum<Integer> {

    STATE_ONE(1, "待审核"),
    STATE_TWO(2, "自动审核通过"),
    STATE_THREE(3, "待人工审核"),
    STATE_FOUR(4, "人工审核拒绝"),
    STATE_FIVE(5, "人工审核通过"),
    STATE_SIX(6, "自动审核拒绝");

    private int value;
    private String desc;

    StateEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.desc;
    }

}
