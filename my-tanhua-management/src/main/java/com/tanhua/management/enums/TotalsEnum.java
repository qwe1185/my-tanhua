package com.tanhua.management.enums;

public enum TotalsEnum {

    WHOLE("all", "全部"),
    REVIEWED("3", "待审核"),
    PASSED("4", "已通过"),
    REJECTED("5", "已驳回");

    private String code;
    private String title;

    TotalsEnum(String code, String title) {
        this.code = code;
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
