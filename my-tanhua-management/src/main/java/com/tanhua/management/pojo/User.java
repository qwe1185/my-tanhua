package com.tanhua.management.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class User extends BasePojo {

    private Long id;
    private String mobile; //手机号

    @JsonIgnore
    private String password; //密码，json序列化时忽略

    private Integer frozenTime; //冻结时间点
    private Integer frozenRange; //冻结时长
    private String frozenReason; //冻结原因
    private String frozenRemark; //冻结备注
    private String thawedReason; //解冻原因

}
