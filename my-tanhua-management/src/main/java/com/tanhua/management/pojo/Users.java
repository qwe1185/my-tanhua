package com.tanhua.management.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("analysis_by_day")
public class Users extends BasePojo {
    @TableId(type = IdType.AUTO)
    private int id;
    @TableField("record_date")
    private Date date;
    @TableField("num_registered")
    private int newUsers;
    @TableField("num_active")
    private int livelyUsers;//活跃用户数
    @TableField("num_login")
    private int loginTimes;//登陆次数
    @TableField("num_retention1d")
    private int retentionld;//次日留存
}
