package com.tanhua.management.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DistributionVo {
    private  List<YearVo> industryDistribution ;
    private  List<YearVo> ageDistribution;
    private  List<YearVo> genderDistribution;
    private  List<YearVo> localDistribution;
    private  List<YearVo> localTotal;
}
