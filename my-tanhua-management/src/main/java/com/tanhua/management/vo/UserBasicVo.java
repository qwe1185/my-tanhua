package com.tanhua.management.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserBasicVo implements Serializable {
    private static final long serialVersionUID = 935456232417502524L;

    private Integer id;
    private String logo;
    private String logoStatus;
    private String nickname;
    private String mobile;
    private String sex;
    private Integer age;
    private String occupation;
    private String userStatus;
    private Long lastActiveTime;
    private String city;
}
