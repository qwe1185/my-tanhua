package com.tanhua.management.vo;

import lombok.Data;

@Data
public class CommentVo {

    private String id; //评论id
    private String nickname; //昵称
    private Long userId; //评论人ID
    private String content; //评论
    private Long createDate; //评论时间: 08:27

}
