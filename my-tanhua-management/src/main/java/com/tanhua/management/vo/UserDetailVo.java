package com.tanhua.management.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailVo implements Serializable {
    private static final long serialVersionUID = 9096178232417502524L;
    private Integer id;
    private String nickname;
    private String mobile;
    private String sex;
    private String personalSignature;
    private Integer age;
    private Integer countBeLiked;
    private Integer countLiked;
    private Integer countMatching;
    private Integer income;
    private String occupation;
    private String userStatus;
    private long created; //修改为long
    private String city;
    private long lastActiveTime; //修改为long
    private String lastLoginLocation;
    private String logo;
    private String tags;
}
