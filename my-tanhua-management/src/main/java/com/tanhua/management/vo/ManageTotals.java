package com.tanhua.management.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ManageTotals {
    private String title; //状态标题
    private String code; //状态代码
    private Integer value; //数量
}
