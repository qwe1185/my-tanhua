package com.tanhua.management.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VideoPages {
    private ObjectId id; //编号
    private String nickname; //发布人
    private Integer userId; //发布人id
    private Integer createDate; //发布时间
    private String videoUrl; //视频URL
    private String picUrl; //封面图片
    private Integer likeCount; //点赞数量
    private Integer commentCount; //评论数量
    private Integer reportCount; //举报数量
    private Integer forwardingCount; //转发数量


}
