package com.tanhua.management.vo;

import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
public class ManageMessages {

    private String id; //编号
    private String nickname; //作者昵称
    private Integer userId; //作者ID
    private String userLogo; //作者头像
    private Long createDate; //发布日期
    private String text; //正文
    private String[] medias; //图片列表
    private Integer state; //审核状态，1为待审核，2为自动审核通过，3为待人工审核，4为人工审核拒绝，5为人工审核通过，6为自动审核拒绝
    private Integer topState; //置顶状态，1为未置顶，2为置顶
    private Integer commentCount; //评论数
    private Long likeCount; //点赞数
    private Integer reportCount; //举报数
    private Integer forwardingCount; //转发数

    private List<?> items = Collections.emptyList(); //列表

}
