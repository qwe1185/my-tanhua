package com.tanhua.management.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminVo implements Serializable {

    private String uid;

    private String username;

    private String avatar;

}
