package com.tanhua.management.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SortProp {
    private Integer likeCount; //点赞数量
    private Integer commentCount; //评论数量
    private Integer reportCount; //举报数量
    private Integer forwardingCount; //转发数量
    private Integer createDate; //发布时间
}
