package com.tanhua.management.controller;

import cn.hutool.json.JSON;
import com.tanhua.management.service.ManageMessagesService;
import com.tanhua.management.vo.ManageMessages;
import com.tanhua.management.vo.PageManageResult;
import com.tanhua.management.vo.PageResult;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("management")
public class ManageMessagesController {

    @Autowired
    private ManageMessagesService manageService;

    /**
     * 消息翻页
     *
     * @param page
     * @param pageSize
     * @param sortProp
     * @param sortOrder
     * @param id
     * @param sd
     * @param ed
     * @param state
     * @return
     */
    @GetMapping("manage/messages")
    public ResponseEntity<PageManageResult> messagesList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                         @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize,
                                                         @RequestParam(value = "sortProp") String sortProp,
                                                         @RequestParam(value = "sortOrder") String sortOrder,
                                                         @RequestParam(value = "id", required = false) String id,
                                                         @RequestParam(value = "uid", required = false) String uid,
                                                         @RequestParam(value = "sd", required = false) String sd,
                                                         @RequestParam(value = "ed", required = false) String ed,
                                                         @RequestParam(value = "state", required = false) String state) {
        try {
            Map<String, String> param = new HashMap<>();
            param.put("sortProp", sortProp);
            param.put("sortOrder", sortOrder);
            param.put("id", id);
            param.put("uid", uid);
            param.put("sd", sd);
            param.put("ed", ed);
            param.put("state", state);
            PageManageResult pageRequest = manageService.messagesList(page, pageSize, param);
            return ResponseEntity.ok(pageRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 消息通过
     *
     * @param id
     * @return
     */
    @PostMapping("manage/messages/pass")
    public ResponseEntity<String> pass(@RequestBody String[] id) {
        try {
            String message = manageService.pass(id);
            return ResponseEntity.ok(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 消息拒绝
     *
     * @param id
     * @return
     */
    @PostMapping("manage/messages/reject")
    public ResponseEntity<String> reject(@RequestBody String[] id) {
        try {
            String message = manageService.reject(id);
            return ResponseEntity.ok(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 消息撤销
     *
     * @param id
     * @return
     */
    @PostMapping("manage/messages/revocation")
    public ResponseEntity<String> revocation(@RequestBody String[] id) {
        try {
            String message = manageService.revocation(id);
            return ResponseEntity.ok(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 消息详情
     *
     * @param id
     * @return
     */
    @GetMapping("manage/messages/{id}")
    public ResponseEntity<ManageMessages> messages(@PathVariable("id") String id) {
        try {
            ManageMessages pageRequest = manageService.messages(id);
            return ResponseEntity.ok(pageRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 消息置顶
     *
     * @param id
     * @return
     */
    @PostMapping("manage/messages/{id}/top")
    public ResponseEntity<String> messagesTop(@PathVariable("id") String id) {
        try {
            String message = manageService.messageTop(id);
            return ResponseEntity.ok(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 消息取消置顶
     *
     * @param id
     * @return
     */
    @PostMapping("manage/messages/{id}/untop")
    public ResponseEntity<String> messagesUntop(@PathVariable("id") String id) {
        try {
            String message = manageService.messageUntop(id);
            return ResponseEntity.ok(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 评论
     *
     * @param page
     * @param pageSize
     * @param sortProp
     * @param sortOrder
     * @param publishId
     * @return
     */
    @GetMapping("manage/messages/comments")
    public ResponseEntity<PageResult> comments(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                               @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize,
                                               @RequestParam(value = "sortProp") String sortProp,
                                               @RequestParam(value = "sortOrder") String sortOrder,
                                               @RequestParam(value = "messageID") ObjectId publishId) {
        PageResult pageResult = manageService.comments(publishId,sortProp,page, pageSize,sortOrder);
        try {
            if (null != pageResult) {
                return ResponseEntity.ok(pageResult);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
