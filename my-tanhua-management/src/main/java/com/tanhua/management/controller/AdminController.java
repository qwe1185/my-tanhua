package com.tanhua.management.controller;

import com.sun.org.apache.bcel.internal.generic.NEW;
import com.tanhua.management.pojo.Admin;
import com.tanhua.management.service.AdminService;
import com.tanhua.management.utils.ValidateCode;
import com.tanhua.management.vo.AdminVo;
import com.tanhua.management.vo.ErrorResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("management")
@Slf4j
public class AdminController {

    @Autowired
    private AdminService adminService;


    /**
     * 用户登录验证码图片
     *
     * @param uuid
     * @param request
     * @param response
     * @return void
     * @date 2021/3/28 0028 9:08
     * @author Administrator
     */
    @GetMapping("system/users/verification")
    public void showValidateCodePic(@RequestParam(name = "uuid") String uuid, HttpServletRequest request, HttpServletResponse response) {

        //响应头
        response.setDateHeader("Expires", 0);
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        ValidateCode validateCode = new ValidateCode();
        //生成图片
        String codePic = validateCode.getRandCodePic(request, response);
        adminService.saveVerification(uuid, codePic);
    }

    /**
     * 用户登录
     *
     * @return
     */
    @PostMapping("system/users/login")
    public ResponseEntity<Object> login(@RequestBody Map<String, String> param) {
        try {
            //获取参数
            String username = param.get("username");
            String password = param.get("password");
            String code = param.get("verificationCode");
            String uuid = param.get("uuid");

            Map map = adminService.login(username, password, code, uuid);
            if (null == map) {

                ErrorResult errorResult = ErrorResult.builder().errCode("000001").errMessage("验证码错误").build();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResult);
            }
            return ResponseEntity.ok(map);
        } catch (Exception e) {
            log.error("登陆失败", e);
            ErrorResult errorResult = ErrorResult.builder().errCode("000002")
                    .errMessage("出现异常,验证码错误!").build();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResult);
        }

    }
    /*
     * 用户基本信息
     * @param    token
     * @return   org.springframework.http.ResponseEntity<com.tanhua.management.vo.AdminVo>
     * @date     2021/3/27 0027 17:17
     * @author   Administrator
     */

    @PostMapping("system/users/profile")
    public ResponseEntity<AdminVo> profile(@RequestHeader("Authorization") String token) {

        try {
            System.out.println(token+"---profile");
            String newToken = token.replace("Bearer ", "");
            AdminVo adminVo = adminService.saveAdminVo(newToken);
            return ResponseEntity.ok(adminVo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 用户登出
     *
     * @param token
     * @return org.springframework.http.ResponseEntity<java.lang.Object>
     * @date 2021/3/28 0028 0:38
     * @author Administrator
     */
    @PostMapping("system/users/loginout")
    public ResponseEntity<Object> loginOut(@RequestHeader("Authorization") String token) {

        try {
            System.out.println(token);
            //String newToken = token.replace("Bearer ", "");
            Boolean bool = adminService.loginOut(token);
            if (bool) {
                return ResponseEntity.ok(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ErrorResult errorResult = ErrorResult.builder().errCode("0000002").errMessage("退出异常!").build();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResult);

    }

    //根据用户名 token
    @GetMapping("system/users/{token}")
    public Admin queryUserByToken(@PathVariable("token") String token) {
        return this.adminService.queryAdminByToken(token);
    }
}
