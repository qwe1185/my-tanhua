package com.tanhua.management.controller;

import com.tanhua.management.service.DashboardService;
import com.tanhua.management.vo.DistributionVo;
import com.tanhua.management.vo.UserActivityVo;
import com.tanhua.management.vo.UsersVo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("management/dashboard")
@Slf4j
public class DashboardController {

    @Autowired
    private DashboardService dashboardService;

    @GetMapping("summary")
    public ResponseEntity<UserActivityVo> summmaryQuery() {

        try {
            UserActivityVo userActivityVo = dashboardService.summmaryQuery();
            if (userActivityVo != null) {
                return ResponseEntity.ok(userActivityVo);
            }
        } catch (Exception e) {
            log.error("查询后台用户数据出错",e);
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }

    @GetMapping("users")
    public ResponseEntity<UsersVo> usersQuery(@RequestParam(value = "sd") Long sd,
                                               @RequestParam(value = "ed") Long ed,
                                               @RequestParam(value = "type") Integer type) {

        try {
            UsersVo usersVo = dashboardService.usersQuery(sd,ed,type);
            if (usersVo != null) {
                return ResponseEntity.ok(usersVo);
            }
        } catch (Exception e) {
            log.error("查询后台用户数据表格出错",e);
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }


    @GetMapping("distribution")
    public ResponseEntity<DistributionVo> distributionQuery(@RequestParam(value = "sd") Long sd,
                                                            @RequestParam(value = "ed") Long ed){
        try {
            DistributionVo distributionVo = dashboardService.distributionQuery(sd,ed);
            if (distributionVo != null) {
                return ResponseEntity.ok(distributionVo);
            }
        } catch (Exception e) {
            log.error("查询后台分布数据出错",e);
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
}
