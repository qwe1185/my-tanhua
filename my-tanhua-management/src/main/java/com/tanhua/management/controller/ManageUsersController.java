package com.tanhua.management.controller;

import com.tanhua.management.service.ManageUsersService;
import com.tanhua.management.vo.PageResult;
import com.tanhua.management.vo.SortProp;
import com.tanhua.management.vo.UserDetailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("management/manage/users")
public class ManageUsersController {

    @Autowired
    private ManageUsersService manageUsersService;

    /**
     * 查询所有用户列表
     *
     * @param pagesize
     * @param page
     * @param id
     * @param nickname
     * @param city
     * @return
     */
    @GetMapping
    public ResponseEntity<PageResult> queryConditionUser(@RequestParam(value = "pagesize", defaultValue = "10", required = false) String pagesize,
                                                         @RequestParam(value = "page", defaultValue = "1", required = false) String page,
                                                         @RequestParam(value = "id", required = false) String id,
                                                         @RequestParam(value = "nickname", required = false) String nickname,
                                                         @RequestParam(value = "city", required = false) String city) {
        try {
            PageResult pageResult = manageUsersService.queryConditionUser(pagesize, page, id, nickname, city);
            return ResponseEntity.status(200).body(pageResult);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(500).build();
    }

    /**
     * 查询单人详细
     *
     * @param userId
     * @return
     */
    @GetMapping("{userId}")
    public ResponseEntity<UserDetailVo> queryUserDetailed(@PathVariable("userId") String userId) {

        try {
            UserDetailVo userDetailVo = manageUsersService.queryUserDetailed(userId);
            return ResponseEntity.status(200).body(userDetailVo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(500).build();
    }

    /**
     * 冻结用户
     *
     * @param param
     * @return
     */
    @PostMapping("freeze")
    public ResponseEntity<Map<String, String>> userFreeze(@RequestBody Map<String, String> param) {

        try {
            Map<String, String> message = manageUsersService.userFreeze(param);
            return ResponseEntity.status(200).body(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(500).build();
    }

    /**
     * 解冻用户
     *
     * @param param
     * @return
     */
    @PostMapping("unfreeze")
    public ResponseEntity<Map<String, String>> userUnfreeze(@RequestBody Map<String, String> param) {

        try {
            Map<String, String> message = manageUsersService.userUnfreeze(param);
            return ResponseEntity.status(200).body(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(500).build();
    }



}
