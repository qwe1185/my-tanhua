package com.tanhua.management.controller;

import com.tanhua.management.service.VideoService;
import com.tanhua.management.vo.PageResult;
import com.tanhua.management.vo.SortProp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("management/manage")
@Slf4j
public class VideoController {
    @Autowired
    private VideoService videoService;
    @GetMapping("videos")
    public ResponseEntity<PageResult> videosPage(@RequestHeader("Authorization")String token,
                                                 @RequestParam("uid") Long uid,
                                                 @RequestParam(value = "page", defaultValue = "1") Integer page,
                                                 @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize,
                                                 @RequestParam("sortProp") String sortProp,
                                                 @RequestParam("sortOrder") String sortOrder ){
        try {
            //Map<String, String> param = new HashMap<>();
            PageResult pageResult = this. videoService.videosPage(token,uid,page,pageSize,sortProp,sortOrder);

            return ResponseEntity.ok(pageResult );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
