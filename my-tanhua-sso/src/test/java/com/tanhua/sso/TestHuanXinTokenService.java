package com.tanhua.sso;

import com.tanhua.sso.service.HuanXinService;
import com.tanhua.sso.service.HuanXinTokenService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestHuanXinTokenService {

    @Autowired
    private HuanXinTokenService huanXinTokenService;

    @Autowired
    private HuanXinService huanXinService;

    @Test
    public void testGetToken(){
        String token = this.huanXinTokenService.getToken();
        System.out.println(token);
    }

    @Test
    public void testRegister(){

        System.out.println(this.huanXinService.register(10L));
        System.out.println(this.huanXinService.register(11L));
    }

}