package com.tanhua.sso.dto;


import lombok.Data;

@Data
public class UserLoginDTO {

    private String phone;
    private String verificationCode;

}
