package com.tanhua.sso.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.tanhua.sso.mapper.UserMapper;
import com.tanhua.sso.pojo.User;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.tanhua.sso.service.SmsService.REDIS_KEY_PREFIX;

@Service
@Slf4j
public class UserService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private UserMapper userMapper;

    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private RocketMQTemplate rocketMQTemplate;


    /**
     * 用户登录
     *
     * @param phone 手机号
     * @param code  验证码
     * @return
     */
    public Map login(String phone, String code) {
        //验证码是否匹配
        String redisKey = REDIS_KEY_PREFIX + phone;
        boolean isNew = false;

        //校验验证码
        String redisData = this.redisTemplate.opsForValue().get(redisKey);
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        if (!StringUtils.equals(code, redisData)) {
            return null; //验证码错误
        }

        //校验成功，将验证码删除
        this.redisTemplate.delete(redisKey);

        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getMobile, phone);
        //手机号是否存在
        User user = userMapper.selectOne(queryWrapper);
        if (user == null) {
            //给新用户注册
            user = new User();
            user.setMobile(phone);
            user.setPassword(DigestUtils.md5Hex("123456"));
            user.setFrozenRange(0);

            //注册新用户
            this.userMapper.insert(user);
            isNew = true;
        } else {
            //判断用户是否已经被冻结,已冻结且未超过冻结时间返回null
            Integer userStatus = user.getFrozenRange();
            if (userStatus == 1) {
                Integer frozenTime = user.getFrozenTime();
                switch (frozenTime) {
                    case 1:
                        //得到冻结时间的毫秒值时间戳
                        long frozenTimeStamp1 = user.getUpdated().getTime();
                        //得到冻结期限的时间戳
                        Long thawedTimeStamp1 = frozenTimeStamp1 + 3 * 24 * 60 * 60 * 1000;
                        //判断冻结期限是否超过当前时间,超过则取消冻结,不超过返回null不允许登录
                        if (thawedTimeStamp1 <= System.currentTimeMillis()) {
                            this.userUnfreeze(user.getId(), "已经到解冻时间");
                        } else {
                            //未超过,返回null
                            return null;
                        }
                        //取消冻结后,跳出循环
                        break;
                    case 2:
                        //得到冻结时间的毫秒值时间戳
                        long frozenTimeStamp2 = user.getUpdated().getTime();
                        //得到冻结期限的时间戳
                        Long thawedTimeStamp2 = frozenTimeStamp2 + 7 * 24 * 60 * 60 * 1000;
                        //判断冻结期限是否超过当前时间,超过则取消冻结,不超过返回null不允许登录
                        if (thawedTimeStamp2 <= System.currentTimeMillis()) {
                            this.userUnfreeze(user.getId(), "已经到解冻时间");
                        } else {
                            //未超过,返回null
                            return null;
                        }
                        //取消冻结后,跳出循环
                        break;
                    case 3:
                        return null;
                    default:
                        return null;
                }
            }
        }


        //返回token
        Map<String, Object> map = new HashMap<>();
        map.put("id", user.getId());
        String token = Jwts.builder().setClaims(map)
                .signWith(SignatureAlgorithm.HS256, secret)
                .setExpiration(new DateTime().plusHours(12).toDate())
                .compact();

        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("token", token);
        hashMap.put("isNew", isNew);


            // 添加发送登录成功的消息 日志 在这里直接发送
            Map<String, Object> msg = new HashMap<>();
            msg.put("userId", user.getId());
            msg.put("type", 01);
            msg.put("date", new Date());
            this.rocketMQTemplate.convertAndSend("tanhua-sso-login", msg);

        return hashMap;
    }


    public User queryUserByToken(String token) {
        try {
            User user = new User();
            // 通过token解析数据
            Map<String, Object> body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
            Long id = Long.valueOf(body.get("id").toString());
            user.setId(id);
            //先查缓存
            String redisKey = "TANHUA_USER_MOBILE_" + id;
            if (this.redisTemplate.hasKey(redisKey)) {
                String mobile = this.redisTemplate.opsForValue().get(redisKey);
                user.setMobile(mobile);
            } else {
                user = userMapper.selectById(id);
                //手机号放到redis，还得和token的过期时间保持一致
                String exp = body.get("exp").toString();//秒为单位
                Long timeout = Long.valueOf(exp) * 1000 - System.currentTimeMillis();
                this.redisTemplate.opsForValue().set(redisKey, user.getMobile(), timeout, TimeUnit.MILLISECONDS);
            }
            return user;
        } catch (Exception e) {
            log.error("解析用户出错", e);
            return null;
        }

    }

    public boolean updatePhone(Long id, String newPhone) {
        //先查询新手机存在？
        LambdaQueryWrapper<User> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userLambdaQueryWrapper.eq(User::getMobile, newPhone);
        if (userMapper.selectCount(userLambdaQueryWrapper) > 0) {
            return false;
        }
        User user = new User();
        user.setId(id);
        user.setMobile(newPhone);

        String redisKey = "TANHUA_USER_MOBILE_" + id;
        Long expire = redisTemplate.getExpire(redisKey);
        this.redisTemplate.opsForValue().set(redisKey, newPhone, expire, TimeUnit.SECONDS);

        return userMapper.updateById(user) > 0;
    }

    //解冻
    public Boolean userUnfreeze(Long userId, String reasonsForThawing) {

        LambdaUpdateWrapper<User> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper
                .eq(User::getId, userId) //修改的userId
                .set(User::getUpdated, new Date()) //更新修改时间
                .set(User::getFrozenTime, null)//置空冻结状态种种
                .set(User::getFrozenRange, 0)
                .set(User::getFrozenReason, null)
                .set(User::getFrozenRemark, null)
                .set(User::getThawedReason, reasonsForThawing); //添加解冻状态

        int result = userMapper.update(null, lambdaUpdateWrapper);

        return result == 1;
    }
}