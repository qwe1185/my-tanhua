package com.tanhua.sso.service;


import com.tanhua.sso.pojo.User;
import com.tanhua.sso.vo.ErrorResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class UsersService {
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private SmsService smsService;

    public boolean sendVerificationCode(String token) {
        User user = this.userService.queryUserByToken(token);
        ErrorResult errorResult = this.smsService.sendCheckCode(user.getMobile());
        return errorResult == null;
    }

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    public Boolean checkVerificationCode(String code, String token) {
        //查询到用户的手机号
        User user = this.userService.queryUserByToken(token);
        if (null == user) {
            return false;
        }

        String redisKey = SmsService.REDIS_KEY_PREFIX + user.getMobile();
        String value = this.redisTemplate.opsForValue().get(redisKey);

        if (StringUtils.equals(value, code)&&StringUtils.isNotEmpty(code)) {
            //验证码正确
            this.redisTemplate.delete(redisKey);
            return true;
        }

        return false;
    }

    public boolean savePhone(String token, String newPhone) {
        User user = this.userService.queryUserByToken(token);
        return this.userService.updatePhone(user.getId(), newPhone);
    }
}