
package com.tanhua.sso.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanhua.sso.config.HuanXinConfig;
import com.tanhua.sso.vo.HuanXinUser;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
//import sun.security.provider.MD5;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HuanXinService {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private HuanXinTokenService huanXinTokenService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private HuanXinConfig huanXinConfig;


/**
     * 注册环信用户
     *
     * @param userId
     * @return
     *//*
*/


    public boolean register(Long userId) {
        try {
            String url = this.huanXinConfig.getUrl()
                    + this.huanXinConfig.getOrgName() + "/"
                    + this.huanXinConfig.getAppName() + "/users";

            String token = huanXinTokenService.getToken();
            //封装请求头
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");
            headers.add("Authorization", "Bearer "+token);

            //包装用户
            HuanXinUser huanXinUser = new HuanXinUser(userId.toString(), DigestUtils.md5Hex(userId + "_itcast_tanhua"));
            List<HuanXinUser> huanXinUsers = Arrays.asList(huanXinUser);

            //封装entity
            HttpEntity<Object> entity = new HttpEntity<>(MAPPER.writeValueAsString(huanXinUsers), headers);
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, entity, String.class);
            return responseEntity.getStatusCodeValue()==200;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return false;
    }




/**
     * 添加好友
     *
     * @param userId
     * @param friendId
     * @return
     *//*
*/


    public boolean contactUsers(String userId, String friendId) {
        String targetUrl = this.huanXinConfig.getUrl()
                + this.huanXinConfig.getOrgName() + "/"
                + this.huanXinConfig.getAppName() + "/users/" +
                userId + "/contacts/users/" + friendId;
        try {
            String token = this.huanXinTokenService.getToken();
            // 请求头
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Bearer " + token);

            HttpEntity<String> httpEntity = new HttpEntity<>(headers);
            ResponseEntity<String> responseEntity = this.restTemplate.postForEntity(targetUrl, httpEntity, String.class);

            return responseEntity.getStatusCodeValue() == 200;
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 添加失败
        return false;
    }

    public boolean sendMsg(String target, String msg, String type) {
        //拼接url
        String url = this.huanXinConfig.getUrl()
                + this.huanXinConfig.getOrgName() + "/"
                + this.huanXinConfig.getAppName() + "/messages";
        try {
            //获取token
            String token = this.huanXinTokenService.getToken();

            //封装请求头
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");
            headers.add("Authorization", "Bearer "+token);

            //封装请求体
            Map<String, Object> param = new HashMap<>();
            param.put("target_type", "users");
            param.put("target", Arrays.asList(target));

            Map<String, Object> msgParam = new HashMap<>();
            msgParam.put("msg", msg);
            msgParam.put("type", type);

            param.put("msg", msgParam);

            //封装entity
            HttpEntity<String> entity = new HttpEntity<>(MAPPER.writeValueAsString(param), headers);

            //发送消息
            ResponseEntity<String> responseEntity = this.restTemplate.postForEntity(url, entity, String.class);

            return responseEntity.getStatusCodeValue()==200;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;


    }
}


