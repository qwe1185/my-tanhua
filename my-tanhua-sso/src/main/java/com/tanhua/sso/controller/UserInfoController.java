package com.tanhua.sso.controller;


import com.tanhua.sso.service.UserInfoService;
import com.tanhua.sso.vo.ErrorResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
@RequestMapping()
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;


    @PostMapping("/user/loginReginfo")
    public ResponseEntity<Object> saveUserInfo(@RequestBody Map<String,String> param,
                                               @RequestHeader("Authorization") String token){
        try {
            boolean b = userInfoService.saveInfo(param, token);
            if (b) {
                return ResponseEntity.ok(null);
            }
        } catch (Exception e) {
            ErrorResult errorResult = ErrorResult.builder().errCode("000001").errMessage("出现异常，保存信息失败").build();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResult);
        }
        ErrorResult errorResult = ErrorResult.builder().errCode("000002").errMessage("用户不合法").build();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResult);

    }

    @PostMapping("/user/loginReginfo/head")
    public ResponseEntity<Object> saveLogo(MultipartFile headPhoto,
                                               @RequestHeader("Authorization") String token) {
        try {
            boolean b = userInfoService.saveLogo(headPhoto, token);
            if (b) {
                return ResponseEntity.ok(null);
            }
        } catch (Exception e) {
            ErrorResult errorResult = ErrorResult.builder().errCode("000001").errMessage("出现异常，保存信息失败").build();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResult);
        }
        ErrorResult errorResult = ErrorResult.builder().errCode("000002").errMessage("用户不合法").build();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResult);
    }
}
