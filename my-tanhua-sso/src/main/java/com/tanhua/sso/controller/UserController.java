package com.tanhua.sso.controller;


import com.tanhua.sso.dto.UserLoginDTO;
import com.tanhua.sso.pojo.User;
import com.tanhua.sso.service.UserService;
import com.tanhua.sso.vo.ErrorResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;


    /**
     * 用户登录
     * @return
     */
    @PostMapping("loginVerification")
    public ResponseEntity<Object> login(@RequestBody UserLoginDTO userLoginDTO) {
        //ErrorResult
        try {
            Map map = this.userService.login(userLoginDTO.getPhone(), userLoginDTO.getVerificationCode());
            if (map == null) {
                ErrorResult errorResult = ErrorResult.builder().errCode("000001").errMessage("验证码错误").build();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResult);
            }
            return ResponseEntity.ok(map);
        } catch (Exception e) {
            ErrorResult errorResult = ErrorResult.builder().errCode("000002").errMessage("出现异常，登录失败").build();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResult);
        }
    }

    /**
     * 校验token，根据token查询用户数据
     *
     * @param token
     * @return
     */
    @GetMapping("{token}")
    public User queryUserByToken(@PathVariable("token") String token) {
        return this.userService.queryUserByToken(token);
    }
}