package com.tanhua.sso.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.sso.pojo.User;
import org.springframework.stereotype.Component;

public interface UserMapper extends BaseMapper<User> {

}