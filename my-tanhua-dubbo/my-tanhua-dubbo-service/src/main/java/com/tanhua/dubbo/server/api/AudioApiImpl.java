package com.tanhua.dubbo.server.api;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.dubbo.config.annotation.Service;
//import com.mongodb.client.result.DeleteResult;
//import com.sun.org.apache.xpath.internal.operations.Bool;
import com.tanhua.dubbo.server.pojo.Audio;

import org.apache.commons.lang3.RandomUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;


@Service(version = "1.0.0")
public class AudioApiImpl implements AudioApi {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    //保存语音
    public Boolean saveAudio(Audio audio) {

        if (audio.getUserId() == null) {
            return false;
        }
        try {
            audio.setId(ObjectId.get());
            audio.setCreated(System.currentTimeMillis());
            audio.setRandom(RandomUtils.nextLong(1, 30000));

            this.mongoTemplate.save(audio);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Audio findAudio(Long id) {
        Query query = Query.query(Criteria.where("userId").is(id));
        List<Audio> list = mongoTemplate.find(query, Audio.class);
        ArrayList<Long> randoms = new ArrayList<>();
        for (Audio audio : list) {
            randoms.add(audio.getRandom());
        }
        Long aLong = RandomUtil.randomEle(randoms);
        Audio audio = mongoTemplate.findOne(Query.query(Criteria.where("random").is(aLong)), Audio.class);
        return audio;
    }

    @Override
    public Boolean deleteAudio(ObjectId id) {
        if (id!=null) {
            Query query = Query.query(Criteria.where("id").is(id));
            this.mongoTemplate.remove(query, Audio.class);
            return true;
        }
        return false;
    }

}
