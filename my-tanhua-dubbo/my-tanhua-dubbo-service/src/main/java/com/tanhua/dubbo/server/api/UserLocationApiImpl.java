package com.tanhua.dubbo.server.api;

import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.pojo.UserLocation;
import com.tanhua.dubbo.server.vo.UserLocationVo;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

@Service(version = "1.0.0")
public class UserLocationApiImpl implements UserLocationApi {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public String updateUserLocation(Long userId, Double longitude, Double latitude, String address) {
        //先查询

        Query query = Query.query(Criteria.where("userId").is(userId));
        UserLocation one = this.mongoTemplate.findOne(query, UserLocation.class);
        if (one != null) {
            Update update = Update.update("location", new GeoJsonPoint(longitude, latitude));
            update.set("address", address);
            update.set("updated", System.currentTimeMillis());
            update.set("lastUpdated", one.getUpdated());

            this.mongoTemplate.updateFirst(query, update, UserLocation.class);
            return one.getId().toHexString();
        } else {
            UserLocation userLocation = new UserLocation();
            userLocation.setAddress(address);
            userLocation.setLocation(new GeoJsonPoint(longitude, latitude));
            userLocation.setUserId(userId);
            userLocation.setId(ObjectId.get());
            userLocation.setCreated(System.currentTimeMillis());
            userLocation.setUpdated(userLocation.getCreated());
            userLocation.setLastUpdated(userLocation.getCreated());

            this.mongoTemplate.save(userLocation);

            return userLocation.getId().toHexString();
        }

    }

    @Override
    public UserLocationVo queryByUserId(Long userId) {
        Query query = Query.query(Criteria.where("userId").is(userId));
        UserLocation one = this.mongoTemplate.findOne(query, UserLocation.class);
        if (one != null) {
            return UserLocationVo.format(one);
        }
        return null;
    }

    @Override
    public List<UserLocationVo> queryUserFromLocation(Double longitude, Double latitude, Integer range) {
        Circle circle = new Circle(new GeoJsonPoint(longitude, latitude), new Distance(range.doubleValue() / 1000, Metrics.KILOMETERS));
        Query query = Query.query(Criteria.where("location").withinSphere(circle));
        List<UserLocation> userLocations = this.mongoTemplate.find(query, UserLocation.class);
        return UserLocationVo.formatToList(userLocations);
    }

    @Override
    public List<UserLocationVo> queryAllUserLocation() {

        List<UserLocation> userLocationList = mongoTemplate.findAll(UserLocation.class);

        return UserLocationVo.formatToList(userLocationList);
    }
}