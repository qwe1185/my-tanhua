package com.tanhua.dubbo.server.api;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.tanhua.dubbo.server.pojo.Comment;
import com.tanhua.dubbo.server.pojo.Publish;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service(version = "1.0.0")
public class ManageMessagesApiImpl implements ManageMessagesApi {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<Publish> publishList(Integer page, Integer pageSize, Map<String, String> param) {
        return mongoTemplate.find(condition(page, pageSize, null, param), Publish.class);
    }

    @Override
    public Integer codeCount(String code, Map<String, String> param) {
        return Math.toIntExact(mongoTemplate.count(condition(null, null, code, param), Publish.class));
    }

    @Override
    public Boolean updateStatus(String[] ids, Integer state) {
        try {
            for (String id : ids) {
                Query query = Query.query(Criteria.where("_id").in(id));
                List<Publish> publishList = mongoTemplate.find(query, Publish.class);
                for (Publish publish : publishList) {
                    if (publish.getState().equals("1") || publish.getState().equals("3")) {
                        Update update = Update.update("state", state);
                        mongoTemplate.updateFirst(query, update, Publish.class);
                    }
                }
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Boolean removePublish(String[] ids) {
        try {
            for (String id : ids) {
                Query query = Query.query(Criteria.where("_id").in(id));
                mongoTemplate.remove(query, Publish.class);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Publish queryPublishById(String id) {
        return mongoTemplate.findById(id, Publish.class);
    }

    @Override
    public Publish queryPublishByTopState(String uid) {
        Query query = Query.query(Criteria.where("userId").is(Integer.valueOf(uid)).and("topState").is(2));
        return mongoTemplate.findOne(query, Publish.class);
    }

    @Override
    public Boolean messageTop(String id) {
        try {
            Query query = Query.query(Criteria.where("topState").is(2));
            Publish publish = mongoTemplate.findOne(query, Publish.class);
            if (publish != null) {
                messageUntop(publish.getId().toHexString());
            }
            Query query1 = Query.query(Criteria.where("_id").is(id));
            Update update = Update.update("topState", 2);
            mongoTemplate.updateFirst(query1, update, Publish.class);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Boolean messageUntop(String id) {
        try {
            Query query = Query.query(Criteria.where("_id").is(id));
            Update update = Update.update("topState", 1);
            mongoTemplate.updateFirst(query, update, Publish.class);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Comment> queryCommentList(ObjectId publishId, String sortProp, Integer page, Integer pageSize, String sortOrder) {
        Query query = Query.query(Criteria.where("publishId").is(publishId).and("commentType").is(2));
        List<Comment> manageCommentList = this.mongoTemplate.find(query, Comment.class);

        if (CollectionUtils.isEmpty(manageCommentList)) {
            return null;
        }

        return manageCommentList;
    }

    //条件
    public Query condition(Integer page, Integer pageSize, String code, Map<String, String> param) {
        Query query = new Query();

        if (StringUtils.isNotEmpty(param.get("id")) && (StringUtils.isEmpty(param.get("sd")) || param.get("sd").equals("-1"))) {
            query.addCriteria(Criteria.where("userId").is(Long.valueOf(param.get("id"))));
        }

        if (StringUtils.isEmpty(param.get("id")) && (StringUtils.isNotEmpty(param.get("sd")) && !param.get("sd").equals("-1"))) {
            query.addCriteria(Criteria.where("created").gte(Long.valueOf(param.get("sd"))).lte(Long.valueOf(param.get("ed"))));
        }

        if (StringUtils.isNotEmpty(param.get("id")) && (StringUtils.isNotEmpty(param.get("sd")) && !param.get("sd").equals("-1"))) {
            query.addCriteria(Criteria.where("userId").is(Long.valueOf(param.get("id"))).and("created").gte(Long.valueOf(param.get("sd"))).lte(Long.valueOf(param.get("ed"))));
        }

        List<Integer> codes;
        Pageable pageable = null;
        if (StringUtils.isEmpty(code) && !StringUtils.equals(code, "userMessagesCount")) {
            if (StringUtils.isNotEmpty(param.get("sortProp")) && param.get("sortOrder").equals("ascending")) {
                pageable = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Order.asc("created")));
            }

            if (StringUtils.isNotEmpty(param.get("sortProp")) && param.get("sortOrder").equals("descending")) {
                pageable = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Order.desc("created")));
            }

            if (StringUtils.isNotEmpty(param.get("state")) && !param.get("state").equals("all")) {
                codes = codes(param.get("state"));
                query.addCriteria(Criteria.where("state").in(codes)).with(pageable);
            }

            if (StringUtils.isNotEmpty(param.get("uid"))) {
                Query query1 = Query.query(Criteria.where("topState").is(2));
                if (mongoTemplate.findOne(query1, Publish.class) != null) {
                    query.addCriteria(Criteria.where("topState").ne(2));
                }
                query.addCriteria(Criteria.where("userId").is(Long.valueOf(param.get("uid")))).with(pageable);
            }
        } else {
            if (StringUtils.isNotEmpty(code) && !code.equals("all") && !code.equals("userMessagesCount")) {
                codes = codes(code);
                query.addCriteria(Criteria.where("state").in(codes));
            }

            if (StringUtils.equals(code, "userMessagesCount")) {
                query.addCriteria(Criteria.where("userId").is(Long.valueOf(param.get("uid"))));
            }
        }

        return query;
    }

    //状态
    public List<Integer> codes(String code) {
        List<Integer> codes = new ArrayList<>();
        if (code.equals("3")) {
            codes.add(1);
            codes.add(3);
        } else if (code.equals("4")) {
            codes.add(2);
            codes.add(5);
        } else {
            codes.add(4);
            codes.add(6);
        }
        return codes;
    }
}