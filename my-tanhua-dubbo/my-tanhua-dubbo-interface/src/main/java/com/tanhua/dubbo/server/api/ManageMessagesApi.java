package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.Comment;
import com.tanhua.dubbo.server.pojo.Publish;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Map;

public interface ManageMessagesApi {

    /**
     * 查询所有动态
     *
     * @param page
     * @param pageSize
     * @param param
     * @return
     */
    List<Publish> publishList(Integer page, Integer pageSize, Map<String, String> param);


    /**
     * 根据状态查询
     *
     * @param param
     * @param code
     * @return
     */
    Integer codeCount(String code, Map<String, String> param);

    /**
     * 修改状态
     *
     * @param id
     * @param state
     * @return
     */
    Boolean updateStatus(String[] id, Integer state);

    /**
     * 撤销动态
     *
     * @param id
     * @return
     */
    Boolean removePublish(String[] id);

    /**
     * 根据用户id查动态详细信息
     *
     * @param uid
     * @return
     */
    Publish queryPublishById(String uid);

    /**
     * 查询置顶动态
     *
     * @return
     */
    Publish queryPublishByTopState(String uid);

    /**
     * 动态置顶
     *
     * @param id
     * @return
     */
    Boolean messageTop(String id);

    /**
     * 动态取消置顶
     *
     * @param id
     * @return
     */
    Boolean messageUntop(String id);

    /**
     * 评论列表
     *
     * @param publishId
     * @return
     */
    List<Comment> queryCommentList(ObjectId publishId, String sortProp, Integer page, Integer pageSize, String sortOrder);
}
