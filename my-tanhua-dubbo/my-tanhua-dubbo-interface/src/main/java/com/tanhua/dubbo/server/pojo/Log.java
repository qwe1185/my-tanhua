package com.tanhua.dubbo.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class Log {

    private int id;//编号
    private int userId;//用户id
    //01为登录，0201为发动态，0202为浏览动态，0203为动态点赞，
    // 0204为动态喜欢，0205为评论，0206为动态取消点赞，0207为动态取消喜欢，
    // 0301为发小视频，0302为小视频点赞，0303为小视频取消点赞，0304为小视频评论
    private Integer type;//状态
    private String logTime;//开始登陆时间
    private String place;//地点
    private String equipment;//设备

}
