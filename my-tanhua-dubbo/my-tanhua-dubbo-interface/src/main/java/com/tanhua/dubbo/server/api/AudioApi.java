package com.tanhua.dubbo.server.api;

import com.tanhua.dubbo.server.pojo.Audio;
import org.bson.types.ObjectId;

public interface AudioApi {

    Boolean saveAudio(Audio audio);

    Audio findAudio(Long id);


    Boolean deleteAudio(ObjectId id);
}
