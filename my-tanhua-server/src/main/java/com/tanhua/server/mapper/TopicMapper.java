package com.tanhua.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.server.pojo.Topic;

/**
 * 问卷_题目_实体
 * @author: gaoLei
 * @createTime: 2021/3/27 15:02
 */
public interface TopicMapper extends BaseMapper<Topic> {

}