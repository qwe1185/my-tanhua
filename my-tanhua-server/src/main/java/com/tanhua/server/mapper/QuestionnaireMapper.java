package com.tanhua.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.server.pojo.Questionnaire;

/**
 * 问卷表 _ 实体
 * @author: gaoLei
 * @createTime: 2021/3/27 15:01
 */
public interface QuestionnaireMapper extends BaseMapper<Questionnaire> {
}