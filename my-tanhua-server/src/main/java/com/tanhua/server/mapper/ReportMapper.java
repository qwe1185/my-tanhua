package com.tanhua.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.server.pojo.Report;

/**
 * @author: gaoLei
 * @createTime: 2021/3/28 14:27
 */
public interface ReportMapper extends BaseMapper<Report> {
}
