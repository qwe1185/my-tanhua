package com.tanhua.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.server.pojo.Raking;

/**
 * @author: gaoLei
 * @createTime: 2021/3/28 11:11
 */
public interface RakingMapper extends BaseMapper<Raking> {
}
