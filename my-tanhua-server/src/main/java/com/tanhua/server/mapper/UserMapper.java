package com.tanhua.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.server.pojo.User;


public interface UserMapper extends BaseMapper<User> {

}
