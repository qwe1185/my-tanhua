package com.tanhua.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.server.pojo.Option;

/**
 * 问卷_题目_选项_实体
 * @author: gaoLei
 * @createTime: 2021/3/27 15:03
 */
public interface OptionMapper  extends BaseMapper<Option> {

}
