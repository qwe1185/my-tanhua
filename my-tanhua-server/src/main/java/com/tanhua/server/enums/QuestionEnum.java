package com.tanhua.server.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 问题的枚举:
 *
 * @author: gaoLei
 * @createTime: 2021/3/27 11:18
 */
public enum QuestionEnum implements IEnum<Integer> {

    /**
     * 问题 1
     */
    QUESTINO1(1,"你何时感觉最好"),
    /**
     * 问题 2
     */
    QUESTINO2(2, "你走路时是"),
    /**
     * 问题 3
     */
    QUESTINO3(3,"和人说话时"),
    /**
     * 问题 4
     */
    QUESTINO4(4,"坐着休息时"),
    /**
     * 问题 5
     */
    QUESTINO5(5,"碰到你感到发笑的事时，你的反应是"),
    /**
     * 问题 6
     */
    QUESTINO6(6,"当你去一个派对或社交场合时"),
    /**
     * 问题 7
     */
    QUESTINO7(7,"当你非常专心工作时，有人打断你，你会"),
    /**
     * 问题 8
     */
    QUESTINO8(8,"下列颜色中，你最喜欢哪一种颜色"),
    /**
     * 问题 9
     */
    QUESTINO9(9,"临入睡的前几分钟，你在床上的姿势是"),
    /**
     * 问题 10
     */
    QUESTINO10(10,"你经常梦到自己在");

    @EnumValue
    private int value;
    @JsonValue
    private String desc;

    QuestionEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.desc;
    }
}
