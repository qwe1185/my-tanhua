package com.tanhua.server.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 维度_值
 *
 * @author: gaoLei
 * @createTime: 2021/3/28 8:46
 */
public enum DimensionValueEnum implements IEnum<Integer> {

    ENGHTY(1,"80%"),
    SEVENTY(2,"70%"),
    NINETY(3,"90"),
    SIXTY(4,"60%");


    @EnumValue
    private int value;
    @JsonValue
    private String desc;

    DimensionValueEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.desc;
    }
}
