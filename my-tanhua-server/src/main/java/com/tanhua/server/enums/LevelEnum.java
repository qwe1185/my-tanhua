package com.tanhua.server.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 问卷列表等级
 *
 * @author: gaoLei
 * @createTime: 2021/3/27 11:15
 */
public enum  LevelEnum implements IEnum<Integer> {

    /**
     * 当前问卷为初级
     */
    PRIMARY(1, "初级"),
    /**
     * 当前问卷为初级
     */
    MIDDLE(2, "中级"),
    /**
     * 当前问卷为初级
     */
    HIGH(3, "高级");

    @EnumValue
    private int value;
    @JsonValue
    private String desc;

    LevelEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.desc;
    }
}