package com.tanhua.server.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 问卷_名称枚举
 *
 * @author: gaoLei
 * @createTime: 2021/3/27 10:42
 */
public enum  QuestionnaireEnum implements IEnum<Integer> {
    /**
     * 初级灵魂题
     */
    PRIMARY(1, "初级灵魂题"),
    /**
     * 中级灵魂题
     */
    MIDDLE(2, "中级灵魂题"),
    /**
     * 高级灵魂题
     */
    HIGH(3, "高级灵魂题");



    @EnumValue
    private int value;
    @JsonValue
    private String desc;

    QuestionnaireEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.desc;
    }
}
