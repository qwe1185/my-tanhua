package com.tanhua.server.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 报告_鉴定图片url
 * @author: gaoLei
 * @createTime: 2021/3/27 11:42
 */
public enum ConclusionPicEnum implements IEnum<Integer> {

    /**
     * 猫头鹰图片
     */
    OWL(1, "https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/owl.png"),
    /**
     * 兔子图片
     */
    RABBIT(2, "https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/rabbit.png"),
    /**
     * 狐狸图片
     */
    FOX(3, "https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/fox.png"),
    /**
     * 狮子图
     */
    LION(4, "https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/lion.png");


    @EnumValue
    private int value;
    @JsonValue
    private String desc;

    ConclusionPicEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.desc;
    }
}