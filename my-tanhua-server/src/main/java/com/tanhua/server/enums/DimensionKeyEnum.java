package com.tanhua.server.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 *  维度项的 key
 * @author: gaoLei
 * @createTime: 2021/3/28 8:40
 */
public enum  DimensionKeyEnum implements IEnum<Integer> {

    OUTGOING(1,"外向"),
    JUDGE(2,"判断"),
    ABSTRACT(3,"抽象"),
    RATIONAL(4,"理性");

    @EnumValue
    private int value;
    @JsonValue
    private String desc;

    DimensionKeyEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.desc;
    }
}