package com.tanhua.server.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 问卷_封面图片sso_url
 * @author: gaoLei
 * @createTime: 2021/3/27 11:03
 */
public enum  CoverEnum implements IEnum<Integer> {

    /**
     * 初级灵魂题_封面url
     */
    PRIMARY(1, "https://t-anhua.oss-cn-shanghai.aliyuncs.com/images/2021/03/12/16155098525915554.jpg"),
    /**
     * 中级灵魂题_封面url
     */
    MIDDLE(2, "https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/qn_cover_02.png"),
    /**
     * 高级灵魂题_封面url
     */
    HIGH(3, "https://tanhua-dev.oss-cn-zhangjiakou.aliyuncs.com/images/test_soul/qn_cover_03.png");


    @EnumValue
    private int value;
    @JsonValue
    private String desc;

    CoverEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.desc;
    }
}
