package com.tanhua.server.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.server.mapper.SettingsMapper;
import com.tanhua.server.pojo.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SettingsService {

    @Autowired
    private SettingsMapper settingsMapper;

    /**
     * 根据用户id查询配置
     * 
     * @param userId
     * @return
     */
    public Settings querySettings(Long userId) {
        QueryWrapper<Settings> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        return this.settingsMapper.selectOne(queryWrapper);
    }

    public void updateNotification(Long id, Boolean likeNotification, Boolean pinglunNotification, Boolean gonggaoNotification) {
        Settings settings = new Settings();
        settings.setUserId(id);
        settings.setLikeNotification(likeNotification);
        settings.setPinglunNotification(pinglunNotification);
        settings.setGonggaoNotification(gonggaoNotification);
        QueryWrapper<Settings> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", id);
        this.settingsMapper.update(settings, queryWrapper);
    }
}
