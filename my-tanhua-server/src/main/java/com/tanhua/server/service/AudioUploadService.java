package com.tanhua.server.service;

import com.aliyun.oss.OSSClient;

import com.tanhua.server.config.AliyunConfig;
import com.tanhua.server.vo.AudioUploadResult;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
@Service
public class AudioUploadService {
    // 允许上传的格式 //mp3、wma、avi、rm、rmvb、flv、mpg、mov、mkv
    private static final String[] AUDIO_TYPE = new String[]{".mp3", ".wma",
            ".avi", ".rm", ".rmvb", ".flv", ".mpg", ".mov", ".m4a", ".mp4"};

    @Autowired
    private OSSClient ossClient;

    @Autowired
    private AliyunConfig aliyunConfig;

    public AudioUploadResult upload(MultipartFile uploadFile) {

        //创建上传结果集对象
        AudioUploadResult audioUploadResult = new AudioUploadResult();
        //校验后缀名 默认是false
        boolean isLegal = false;
        for (String type : AUDIO_TYPE) {
            if (StringUtils.endsWithIgnoreCase(uploadFile.getOriginalFilename(),
                    type)) {
                isLegal = true;
                break;
            }
        }
        //格式不符合  错误信息
        if (!isLegal) {
            audioUploadResult.setStatus("error");
            return audioUploadResult;
        }

        // 文件新路径
        String fileName = uploadFile.getOriginalFilename();
        String filePath = getFilePath(fileName);

        // 上传到阿里云
        try {
            // 目录结构：images/2018/12/29/xxxx.jpg
            ossClient.putObject(aliyunConfig.getBucketName(), filePath, new
                    ByteArrayInputStream(uploadFile.getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
            //上传失败
            audioUploadResult.setStatus("error");
            return audioUploadResult;
        }

        //上传成功
        audioUploadResult.setStatus("done");
        audioUploadResult.setName(this.aliyunConfig.getUrlPrefix() + filePath);
        audioUploadResult.setUid(String.valueOf(System.currentTimeMillis()));

        return audioUploadResult;

    }

    //上传路径
    private String getFilePath(String sourceFileName) {
        DateTime dateTime = new DateTime();
        return "voice/" + dateTime.toString("yyyy")
                + "/" + dateTime.toString("MM") + "/"
                + dateTime.toString("dd") + "/" + System.currentTimeMillis() +
                RandomUtils.nextInt(100, 9999) + "." +
                StringUtils.substringAfterLast(sourceFileName, ".");
    }
}
