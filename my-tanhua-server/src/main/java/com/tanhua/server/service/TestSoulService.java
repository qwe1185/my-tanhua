package com.tanhua.server.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.tanhua.server.enums.*;
import com.tanhua.server.mapper.*;
import com.tanhua.server.pojo.*;
import com.tanhua.server.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * 测灵魂_服务
 *
 * @author: gaoLei
 * @createTime: 2021/3/27 16:30
 */

@Service
public class TestSoulService {

    /**
     * 注入questionnaireMapper
     */
    @Autowired
    private QuestionnaireMapper questionnaireMapper;

    // 注入题目Mapper
    @Autowired
    private TopicMapper topicMapper;

    // 注入选项 Mapper
    @Autowired
    private OptionMapper optionMapper;

    // 注入排名表mapper
    @Autowired
    private RakingMapper rakingMapper;

    // 报告
    @Autowired
    private ReportMapper reportMapper;

    @Autowired
    private UserInfoService userInfoService;

    /**
     * 根据此对象方法,校验token
     */
    @Autowired
    private UserService userService;

    /**
     * 查询问卷集合
     * @param token
     * @return java.util.List<com.tanhua.server.pojo.Questionnaire>
     */
        public List<QuestionnaireVo> queryQuestionnaireList(String token) {

        // 不管啥方法先校验 token(根据id查询user是否存在)
        User user = this.userService.queryUserByToken(token);
        if(null == user){
            // token非法或已过期
            return null;
        }


        // 查询所有表单
        QueryWrapper<Questionnaire> queryWrapper = new QueryWrapper<>();
        List<Questionnaire> questionnaires = questionnaireMapper.selectList(queryWrapper);

        /** 遍历三张表单
         *  填充题目集合
         */
            // 创建容器
            List<QuestionnaireVo> questionnaireVos = new ArrayList<>();

            for (Questionnaire questionnaire : questionnaires) {

            // 获取当前表单的id
            String questionnaireId = questionnaire.getId();
            // 根据表单id,查询问题表中所有属于此表单的题目
            QueryWrapper<Topic> topicQueryWrapper = new QueryWrapper<>();
            topicQueryWrapper.eq("questionnaire_id", questionnaireId);
            // 查询题目集合
            List<Topic> questions = topicMapper.selectList(topicQueryWrapper);

            // 遍历题目集合 取出题目id, 根据此id 查询出题目选项集合 赋值给此到题目的 optionList 属性
            for (Topic topic : questions) {
                String topicId = topic.getId();
                // 创建查询条件: 选项表中的所有 对应的问题id 集合
                QueryWrapper<Option> optionQueryWrapper = new QueryWrapper<>();
                optionQueryWrapper.eq("topic_id", topicId);
                // 查询题目的所有选项
                List<Option> options = optionMapper.selectList(optionQueryWrapper);
                // 放到题目的 OptionList 属性中
                topic.setOptions(options);
            }
            // 将表单的题目集合填充
            questionnaire.setQuestions(questions);

            String s = questionnaire.getCover().toString();

            if("1".equals(questionnaire.getId())){
                questionnaire.setIsLock(0);
            }else if("2".equals(questionnaire.getId())){

                QueryWrapper<Report> reportQueryWrapper = new QueryWrapper<>();
                reportQueryWrapper.eq("apple",1).eq("user_id",user.getId());
                List<Report> reports = reportMapper.selectList(reportQueryWrapper);

                if (!CollectionUtils.isEmpty(reports)){
                    questionnaire.setIsLock(0);
                }

            }
            else if ("3".equals(questionnaire.getId())){
                QueryWrapper<Report> reportQueryWrapper = new QueryWrapper<>();
                reportQueryWrapper.eq("apple",2).eq("user_id",user.getId());
                List<Report> reports = reportMapper.selectList(reportQueryWrapper);

                if (!CollectionUtils.isEmpty(reports)){
                    questionnaire.setIsLock(0);
                }

            }


                QuestionnaireVo questionnaireVo = new QuestionnaireVo();

            questionnaireVo.setId(questionnaire.getId());
            questionnaireVo.setCover(questionnaire.getCover().toString());
            questionnaireVo.setIsLock(questionnaire.getIsLock());
            questionnaireVo.setLevel(questionnaire.getLevel().toString());
            questionnaireVo.setName(questionnaire.getName().toString());
            questionnaireVo.setStar(questionnaire.getStart());
            questionnaireVo.setQuestions(questionnaire.getQuestions());

            questionnaireVos.add(questionnaireVo);
            }

        // 返回填充的数据
        return questionnaireVos;


    }

    /**
     * 创建 一个报告对象, 根据用户 回答,得出相应结果,填充到报告表中,返回报告id
     *
     * @param
     * @return
     */
    public String getReportId(String token,Map<String, List<Answer>> map  ) {


        // 不管啥方法先校验 token(根据id查询user是否存在)
        User user = this.userService.queryUserByToken(token);
        if(null == user){
            // token非法或已过期
            return null;
        }

        // 创建报告对象
        Report report = new Report();


        //获取 题目和选项的集合
        ArrayList<String> optionIdlist = new ArrayList<>();
        ArrayList<String> topicIdList = new ArrayList<>();
        List<Answer> answers = map.get("answers");

        for (Answer answer : answers) {
            topicIdList.add(answer.getQuestionId());
            optionIdlist.add(answer.getOptionId());
        }

        // 获取此次用户提交问卷的等级
        String topicId = topicIdList.get(0);
        Topic topic = topicMapper.selectById(topicId);
        Integer questionnaireId = topic.getQuestionnaireId();
        Questionnaire questionnaire = questionnaireMapper.selectById(questionnaireId);
        Integer level = questionnaire.getLevel().getValue();


        //遍历选项集合ID ,获取所有选项对象,根据对象集合统计分值,根据分值为报告填充相关数据
        List<Option> optionList = new ArrayList<Option>();

        for (String s : optionIdlist) {
            Option option = optionMapper.selectById(s);
            optionList.add(option);
        }


        // 遍历oList,取出分值并统计
        int score = 0;
        for (Option option : optionList) {
            score += option.getScore();
        }
        if (score < 21) {
            report.setFox(1);
            report.setCover(ConclusionPicEnum.OWL);
            report.setConclusion(ConclusionEnum.OWL);
        } else if (score >= 21 && score <= 40) {
            report.setCover(ConclusionPicEnum.RABBIT);
            report.setConclusion(ConclusionEnum.RABBIT);
            report.setFox(2);
        } else if(score >= 41 && score <= 55){
            report.setCover(ConclusionPicEnum.FOX);
            report.setConclusion(ConclusionEnum.FOX);
            report.setFox(3);
        }else {
            report.setCover(ConclusionPicEnum.LION);
            report.setConclusion(ConclusionEnum.LION);
            report.setFox(4);
        }


        report.setUserId(user.getId().intValue()); // 标明当前报告是是谁的
        report.setApple(level);  // 标明是哪个等级的问卷
        report.setScore(score); // 设置分数


        // 查询报告表中是否有当前用户对应等级问卷的报告, 有更新, 没有插入
        QueryWrapper<Report> reportQueryWrapper = new QueryWrapper<>();
        reportQueryWrapper.eq("user_id",user.getId()).eq("apple",level);

        // 删除再保存
        reportMapper.delete(reportQueryWrapper);

        reportMapper.insert(report);

        //再次查询:
        List<Report> reports1 = reportMapper.selectList(reportQueryWrapper);
        return reports1.get(0).getId();


    }

    /**
     * 根据reportId 返回需求的数据集合
     *
     * @param reportId
     * @return com.tanhua.server.pojo.Report
     */
    public ReportVo getReport(String token, Integer reportId) {


        // 不管啥方法先校验 token(根据id查询user是否存在)
        User user = this.userService.queryUserByToken(token);
        if(null == user){
            // token非法或已过期
            return null;
        }

        // 创建容器
        ReportVo reportVo = new ReportVo();
        Report report = reportMapper.selectById(reportId);

        reportVo.setCover(report.getCover().toString());
        reportVo.setConclusion(report.getConclusion().toString());

        // 根据分值区间设置维度值
        Integer score = report.getScore();
        if (score < 21) {


            // 维度值设置
            ArrayList<Dimension> dimensions = new ArrayList<>();
            for (int i = 0; i <= 3 ; i++) {
                Dimension dimension = new Dimension();
                dimension.setKey(DimensionKeyEnum.OUTGOING.toString());
                dimension.setValue(DimensionValueEnum.ENGHTY.toString());
                dimensions.add(dimension);
            }

            reportVo.setDimensions(dimensions);

        } else if (score >= 21 && score <= 40) {


            // 维度值设置
            ArrayList<Dimension> dimensions = new ArrayList<>();
            for (int i = 0; i <= 3 ; i++) {
                Dimension dimension = new Dimension();
                dimension.setKey(DimensionKeyEnum.OUTGOING.toString());
                dimension.setValue(DimensionValueEnum.ENGHTY.toString());
                dimensions.add(dimension);
            }

            reportVo.setDimensions(dimensions);

        } else if (score >= 41 && score <= 55) {



            // 维度值设置
            ArrayList<Dimension> dimensions = new ArrayList<>();
            for (int i = 0; i <= 3 ; i++) {
                Dimension dimension = new Dimension();
                dimension.setKey(DimensionKeyEnum.OUTGOING.toString());
                dimension.setValue(DimensionValueEnum.ENGHTY.toString());
                dimensions.add(dimension);
            }

            reportVo.setDimensions(dimensions);

        } else {


            // 维度值设置
            ArrayList<Dimension> dimensions = new ArrayList<>();
            for (int i = 0; i <= 3 ; i++) {
                Dimension dimension = new Dimension();
                dimension.setKey(DimensionKeyEnum.OUTGOING.toString());
                dimension.setValue(DimensionValueEnum.ENGHTY.toString());
                dimensions.add(dimension);
            }

            reportVo.setDimensions(dimensions);
        }

        // 相似的人
        ArrayList<Long> longs = new ArrayList<>();
        longs.add(1L);
        longs.add(2L);
        longs.add(3L);
        longs.add(4L);
        longs.add(5L);
        longs.add(6L);
        longs.add(7L);
        longs.add(8L);
        longs.add(9L);
        longs.add(10L);

        Collections.shuffle(longs);

        List<Simslar> similarUserVos = new ArrayList<>();

        for (Long userId : longs) {
            UserInfo userInfo = userInfoService.queryUserInfoByUserId(userId);

            Simslar similarUserVo = new Simslar();

            similarUserVo.setId(userInfo.getId().intValue());
            similarUserVo.setAvatar(userInfo.getLogo());

            similarUserVos.add(similarUserVo);
        }

        reportVo.setSimilarYou(similarUserVos);

        return reportVo;
    }
}