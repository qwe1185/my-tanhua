package com.tanhua.server.service;

import com.tanhua.server.pojo.User;
import com.tanhua.server.utils.UserThreadLocal;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class LogSendService {
    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    private static final Logger LOGGER = LoggerFactory.getLogger(LogSendService.class);

    /*
     * 用来发送不同的日志信息到日志管理层
     *
     * */
    public Boolean logSendMsg(String type) {

        try {
            User user = UserThreadLocal.get();
            //构建消息对象
            Map<String, Object> msg = new HashMap<>();
            msg.put("userId", user.getId());
            msg.put("type", type);
            msg.put("date", new Date());


            //发送到log管理
            this.rocketMQTemplate.convertAndSend("tanhua-log", msg);
        } catch (Exception e) {
            LOGGER.error("发送消息失败! " + ", type = " + type, e);
            return false;
        }
        return true;
    }

}
