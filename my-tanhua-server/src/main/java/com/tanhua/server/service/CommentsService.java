package com.tanhua.server.service;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.dubbo.server.api.QuanZiApi;
import com.tanhua.dubbo.server.pojo.Comment;
import com.tanhua.server.pojo.User;
import com.tanhua.server.pojo.UserInfo;
import com.tanhua.server.utils.UserThreadLocal;
import com.tanhua.server.vo.Comments;
import com.tanhua.server.vo.PageResult;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CommentsService {

    @Reference(version = "1.0.0")
    private QuanZiApi quanZiApi;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private UserService userService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    public PageResult queryCommentsList(String publishId, Integer page, Integer pageSize) {

        User user = UserThreadLocal.get();

        PageResult pageResult = new PageResult();
        pageResult.setPage(page);
        pageResult.setPagesize(pageSize);

        List<Comment> records = this.quanZiApi.queryCommentList(publishId, page, pageSize);

        if (CollectionUtils.isEmpty(records)) {
            return pageResult;
        }

        Set<Long> userIds = new HashSet<>();
        for (Comment record : records) {
            userIds.add(record.getUserId());
        }

        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("user_id", userIds);
        List<UserInfo> userInfoList = this.userInfoService.queryUserInfoList(queryWrapper);

        List<Comments> commentsList = new ArrayList<>();
        for (Comment record : records) {
            for (UserInfo userInfo : userInfoList) {
                if (record.getUserId().longValue() == userInfo.getUserId().longValue()) {
                    Comments comments = new Comments();
                    comments.setId(record.getId().toHexString());
                    comments.setCreateDate(new DateTime(record.getCreated()).toString("yyyy年MM月dd日 HH:mm"));
                    comments.setContent(record.getContent());
                    comments.setNickname(userInfo.getNickName());
                    comments.setAvatar(userInfo.getLogo());

                    String likeUserCommentKey = "QUANZI_COMMENT_LIKE_USER_" + user.getId() + "_" + comments.getId();
                    comments.setHasLiked(this.redisTemplate.hasKey(likeUserCommentKey) ? 1 : 0); //是否点赞

                    String likeCommentKey = "QUANZI_COMMENT_LIKE_" + comments.getId();
                    String value = this.redisTemplate.opsForValue().get(likeCommentKey);
                    if (StringUtils.isNotEmpty(value)) {
                        comments.setLikeCount(Integer.valueOf(value)); //点赞数
                    } else {
                        comments.setLikeCount(0); //点赞数
                    }

                    commentsList.add(comments);
                    break;
                }
            }

        }

        pageResult.setItems(commentsList);

        return pageResult;
    }

    public Boolean saveComments(String publishId, String content) {
        User user = UserThreadLocal.get();
        //判断是否有权限评论
        User u = userService.queryUserById(user.getId());
        if (u.getFrozenRange() == 2) {
            Integer frozenTime = user.getFrozenTime();
            switch (frozenTime) {
                case 1:
                    //得到冻结时间的毫秒值时间戳
                    long frozenTimeStamp1 = user.getUpdated().getTime();
                    //得到冻结期限的时间戳
                    Long thawedTimeStamp1 = frozenTimeStamp1 + 3 * 24 * 60 * 60 * 1000;
                    //判断冻结期限是否超过当前时间,超过则取消冻结,不超过返回false不允许评论
                    if (thawedTimeStamp1 <= System.currentTimeMillis()) {
                        userService.userUnfreeze(user.getId(), "已经到解冻时间");
                    } else {
                        //未超过,返回false
                        return false;
                    }
                    //取消冻结后,跳出循环
                    break;
                case 2:
                    //得到冻结时间的毫秒值时间戳
                    long frozenTimeStamp2 = user.getUpdated().getTime();
                    //得到冻结期限的时间戳
                    Long thawedTimeStamp2 = frozenTimeStamp2 + 7 * 24 * 60 * 60 * 1000;
                    //判断冻结期限是否超过当前时间,超过则取消冻结,不超过返回false不允许评论
                    if (thawedTimeStamp2 <= System.currentTimeMillis()) {
                        userService.userUnfreeze(user.getId(), "已经到解冻时间");
                    } else {
                        //未超过,返回false
                        return false;
                    }
                    //取消冻结后,跳出循环
                    break;
                case 3:
                    return false;
                default:
                    return false;
            }
        }

        return this.quanZiApi.saveComment(user.getId(), publishId, 2, content);
    }
}