package com.tanhua.server.service;

import com.alibaba.dubbo.config.annotation.Reference;

import com.tanhua.dubbo.server.api.AudioApi;
import com.tanhua.dubbo.server.pojo.Audio;
import com.tanhua.server.pojo.User;
import com.tanhua.server.pojo.UserInfo;
import com.tanhua.server.utils.UserThreadLocal;
import com.tanhua.server.vo.AudioUploadResult;
import com.tanhua.server.vo.AudioVo;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;

@Service
public class AudioService {

    @Autowired
    private AudioUploadService audioUploadService;

    @Reference(version = "1.0.0")
    private AudioApi audioApi;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private UserInfoService userInfoService;


    //保存发送语音
    public Boolean saveAudio(MultipartFile audioFile) {

        //获得用户
        User user = UserThreadLocal.get();

        //创建音频对象
        Audio audio = new Audio();
        audio.setUserId(user.getId());

        AudioUploadResult audioUploadResult = audioUploadService.upload(audioFile);
        audio.setSoundUrl(audioUploadResult.getName());

        this.audioApi.saveAudio(audio);

        return true;
    }

    //获取语音
    public AudioVo getAudio() {
        User user = UserThreadLocal.get();
        //创建redis语音键(传音只能8次)
        //在redis里设置一个唯一的缓存键，根据当天的时间设置，并且根据下面的清除缓存设置清除
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateTime = simpleDateFormat.format(date);
        String key = user.getId() + "_" + dateTime + "_SEND_SOUND";

        //判断
        String keyValue = redisTemplate.opsForValue().get(key);
        if (null == keyValue) {
            redisTemplate.opsForValue().set(key, "8", Duration.ofDays(1));
        } else if (keyValue.equals("0")) {
            redisTemplate.opsForValue().set(key, "8", Duration.ofDays(1));
        } else {
            redisTemplate.opsForValue().decrement(key);
            //redisTemplate.boundValueOps(key).increment(-1L);
        }


        AudioVo audioVo = new AudioVo();
        //1.调用api随机获得一个audio对象
        Audio audio = this.audioApi.findAudio(user.getId());
        //2.封装成vo
        //2.1audio有的属性进行封装

        audioVo.setId(Math.toIntExact(audio.getUserId()));

        audioVo.setSoundUrl(audio.getSoundUrl());
        //2.2封装userinfo里 的属性
        UserInfo userInfo = userInfoService.queryById(audio.getUserId());
        audioVo.setAge(userInfo.getAge());
        audioVo.setAvatar(userInfo.getLogo());
        audioVo.setNickname(userInfo.getNickName());
        audioVo.setGender(userInfo.getSex().toString());
        String s = redisTemplate.opsForValue().get(key);
        Integer count = Integer.valueOf(s);
        audioVo.setRemainingTimes(count);

        //3.audio删掉
        this.audioApi.deleteAudio(audio.getId());

        return audioVo;
    }
}
