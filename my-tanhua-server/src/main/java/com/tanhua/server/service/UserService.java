package com.tanhua.server.service;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.tanhua.server.mapper.UserMapper;
import com.tanhua.server.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

@Service
@Slf4j
public class UserService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UserMapper userMapper;

    @Value("${tanhua.sso.url}")
    private String tanhuaSSOurl;

    public User queryUserByToken(String token) {
        try {
            //拼接url
            String url = tanhuaSSOurl+"/user/"+token;

            User user = restTemplate.getForObject(url, User.class);
            System.out.println(user);
            return user;
        } catch (RestClientException e) {
            log.error("查询sso，获取user失败");
        }
        return null;
    }

    public User queryUserById(Long userId) {

        return userMapper.selectById(userId);
    }

    //解冻
    public Boolean userUnfreeze(Long userId, String reasonsForThawing) {

        LambdaUpdateWrapper<User> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper
                .eq(User::getId, userId) //修改的userId
                .set(User::getUpdated, new Date()) //更新修改时间
                .set(User::getFrozenTime, null)//置空冻结状态种种
                .set(User::getFrozenRange, 0)
                .set(User::getFrozenReason, null)
                .set(User::getFrozenRemark, null)
                .set(User::getThawedReason, reasonsForThawing); //添加解冻状态

        int result = userMapper.update(null, lambdaUpdateWrapper);

        return result == 1;
    }

}
