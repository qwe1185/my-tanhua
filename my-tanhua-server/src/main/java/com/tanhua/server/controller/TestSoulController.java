package com.tanhua.server.controller;

import com.tanhua.server.pojo.Questionnaire;
import com.tanhua.server.service.TestSoulService;
import com.tanhua.server.vo.Answer;
import com.tanhua.server.vo.QuestionnaireVo;
import com.tanhua.server.vo.ReportVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 测灵魂
 * @author: gaoLei
 * @createTime: 2021/3/27 16:27
 */
@RestController
@RequestMapping("/testSoul")
public class TestSoulController {

    @Autowired
    private TestSoulService testSoulService;

    /**
     * 查询所有问卷
     * @param
     * @return java.util.List<com.tanhua.server.pojo.Questionnaire>
     */
    @GetMapping
    public ResponseEntity<List<QuestionnaireVo>> queryQuestionnaireList(@RequestHeader("Authorization") String token ){

        try {
            List<QuestionnaireVo> list = this.testSoulService.queryQuestionnaireList(token);
            return ResponseEntity.ok(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 获取用户发来的问卷 的  试题编号和选项编号 集合
     * @param answers
     * @return
     */
    @PostMapping
    public ResponseEntity<String> returnReportId(@RequestHeader("Authorization") String token, @RequestBody Map<String , List<Answer> >  answers){
        try {

            String  id =  this.testSoulService.getReportId(token,answers);
            return ResponseEntity.ok(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    /**
     * 根据 报告id, 返回
     * @param reportId
     * @return
     */
    @GetMapping("report/{id}")
    public ResponseEntity<ReportVo> getReport(@RequestHeader("Authorization") String token, @PathVariable("id") Integer reportId){

        try {
            ReportVo reportVo = this.testSoulService.getReport(token, reportId);
            return ResponseEntity.ok(reportVo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}