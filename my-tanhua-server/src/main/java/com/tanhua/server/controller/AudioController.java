package com.tanhua.server.controller;


import com.tanhua.dubbo.server.pojo.Audio;
import com.tanhua.server.service.AudioService;
import com.tanhua.server.vo.AudioVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("peachblossom")
public class AudioController {
    @Autowired
    private AudioService audioService;

    @PostMapping
    public ResponseEntity<Void> saveVideo(@RequestParam(value = "soundFile") MultipartFile audioFile) {

        boolean b = this.audioService.saveAudio(audioFile);
        if (b) {
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }


    @GetMapping
    public ResponseEntity<AudioVo> getAudio(){

        try {

            AudioVo audioVo = this.audioService.getAudio();
            if (audioVo!=null) {

                return ResponseEntity.ok(audioVo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }


}
