package com.tanhua.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 性格分数排名表
 * @author: gaoLei
 * @createTime: 2021/3/28 10:56
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Raking {

    private Integer id; // userId
    private Integer score; // 性格分数

}