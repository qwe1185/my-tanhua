package com.tanhua.server.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.tanhua.server.enums.QuestionEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 问卷_题目
 * @author: gaoLei
 * @createTime: 2021/3/27 14:36
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Topic {

    /**
     * 试题编号
     */
    private String id;
    /**
     * 问题描述
     */
    private QuestionEnum question;

    /**
     * 选项集合
     */
    @TableField(exist = false )
    private List<Option> options;
    /**
     *  此题归属于 哪张问卷 表问卷id
     */
    private Integer  questionnaireId;

}