package com.tanhua.server.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.tanhua.server.enums.ConclusionEnum;
import com.tanhua.server.enums.ConclusionPicEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 问卷_报告
 * @author: gaoLei
 * @createTime: 2021/3/28 8:33
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Report {

    @TableId("id")
    private String id; //报告id
    private ConclusionEnum conclusion; //鉴定结果
    private ConclusionPicEnum cover; //鉴定图片的url

    @TableField(value = "`user_id`")
    private Integer userId; // 当前生成报告对应的用户id

    private Integer apple; // 问卷等级,三个等级的问卷,每个用户都会生成对应的一份报告

    private Integer score; // 报告得分

    private Integer fox; // 1~4 代表着 性格分类 low/rabbit/fox/lion



}