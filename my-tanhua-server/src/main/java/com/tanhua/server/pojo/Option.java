package com.tanhua.server.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 问卷_选项
 *
 * @author: gaoLei
 * @createTime: 2021/3/27 14:40
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Option {

    /**
     * 归属于哪一条题目
     */
    private String topicId;
    /**
     * 选项编号
     */
    private String id;
    /**
     * 选项内容
     */
    @TableField(value = "`option`")
    private String option;
    /**
     * 当前选项分值;
     */
    private Integer score;

}