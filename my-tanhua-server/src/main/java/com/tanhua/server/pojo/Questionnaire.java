package com.tanhua.server.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.tanhua.server.enums.CoverEnum;
import com.tanhua.server.enums.LevelEnum;
import com.tanhua.server.enums.QuestionnaireEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 问卷实体类
 * @author: gaoLei
 * @createTime: 2021/3/27 12:01
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Questionnaire {

    /**
     * 问卷编号
     */
    private String id;
    /**
     * 问卷名称
     */
    private QuestionnaireEnum name;
    /**
     * 问卷封面
     */
    private CoverEnum cover;
    /**
     * 问卷等级
     */
    private LevelEnum level;
    /**
     * 问卷星级 2~5 星
     */
    private Integer start;
    /**
     * 是否上锁  0(解锁) // 1(上锁)
     */
    private Integer isLock;
    /**
     * 最新报告id
     */
    private String reportId;
    /**
     * 当前问卷的 所有题目 合集
     */
    @TableField(exist = false )
    List<Topic> questions;



}