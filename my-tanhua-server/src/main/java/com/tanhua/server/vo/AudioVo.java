package com.tanhua.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AudioVo {

    private Integer id;
    private String avatar;//头像
    private String nickname;//昵称
    private String gender;//性别
    private Integer age;//年龄
    private String soundUrl;//路径
    private Integer remainingTimes;//剩余次数


}
