package com.tanhua.server.vo;

import com.tanhua.server.pojo.Topic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestionnaireVo {

    private String id;
    private String name;
    private String cover;
    private String level;
    private Integer star;
    List<Topic> questions;
    private Integer isLock;

}
