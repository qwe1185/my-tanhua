package com.tanhua.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author: gaoLei
 * @createTime: 2021/3/28 16:20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportVo {

    private String conclusion; // 鉴定结果
    private String cover; // 鉴定图片url

    private List<Dimension> dimensions; // 维度值
    private List<Simslar>  similarYou; // 相似人的集合



}