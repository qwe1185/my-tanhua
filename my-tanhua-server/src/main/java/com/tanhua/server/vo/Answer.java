package com.tanhua.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: gaoLei
 * @createTime: 2021/3/28 22:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Answer {

    private String questionId;
    private String optionId;
}