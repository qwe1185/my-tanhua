package com.tanhua.recommend.msg;


import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.RandomUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.tanhua.recommend.mapper.LogMapper;
import com.tanhua.recommend.pojo.Log;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;

@Component
@RocketMQMessageListener(topic = "tanhua-log",//对应发送端即可
        consumerGroup = "tanhua-log-Server")
@Slf4j
public class LogMsgConsumer implements RocketMQListener<String> {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private LogMapper logMapper;

    // 模拟手机型号
    private static final String[] mockDevices = {
            "华为荣耀P30", "华为荣耀P29", "华为荣耀P28", "华为荣耀P27", "华为荣耀P26", "华为荣耀P25"
    };
    // 模拟操作地点
    private static final String[] mockCities = {
            "北京", "上海", "广州", "深圳", "天津", "石家庄"
    };

    @Override
    public void onMessage(String msg) {
        try {
            JsonNode jsonNode = MAPPER.readTree(msg);
            long userId = jsonNode.get("userId").asLong();
            Integer type = jsonNode.get("type").asInt();
            Date date = Convert.toDate(jsonNode.get("date").asText(), null);

            //准备日志对象
            Log log = new Log();
            log.setUserId((int) userId);//强转一下
            log.setType(type);
            log.setPlace(mockCities[RandomUtil.randomInt(0, mockCities.length - 1)]);//假的 随机一个
            log.setEquipment(mockDevices[RandomUtil.randomInt(0, mockDevices.length - 1)]);//假的 随机一个
            log.setCreated(date);
            //存入
            this.logMapper.insert(log);

        } catch (IOException e) {
            log.error("处理消息失败！msg = " + msg);
        }
    }
}
