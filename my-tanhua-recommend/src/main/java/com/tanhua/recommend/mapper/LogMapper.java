package com.tanhua.recommend.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.recommend.pojo.Log;
import org.springframework.stereotype.Repository;

@Repository
public interface LogMapper extends BaseMapper<Log> {

}
